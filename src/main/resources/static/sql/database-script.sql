INSERT INTO public.role(
    id, name)
VALUES (1, 'DOC'),
       (2, 'EMP');

INSERT INTO public.branch(
    id, address,flag, name, note, phone_number)
VALUES ('CN00001', '24 B3 Mai Dịch 217 Cầu Giấy ,Hà nội,Việt Nam',0
       ,'Phòng Khám Mai Dịch' , 'Fax : 024 3972 4568', '+84 586935079'),
       ('CN00002', '202 Trương Định,Hoàng Mai ,Hà nội,Việt Nam',0
       , 'Phòng Khám Trương Định', 'Fax : N/A', '+84 586512274'),
       ('CN00003', '339 Nguyễn Văn Lương ,Hồ Chí Minh ,Việt Nam',0
       , 'Phòng Khám Nguyễn Văn Lương', 'Fax : N/A', '+84 586072996'),
       ('CN00004', 'Số 14 Hai Bà Trưng, Hà Nội',0
       , 'Phòng Khám Hai Bà Trưng','Fax : N/A', '+84 367778384'),
       ('CN00005', 'Số 268 Ngọc Hà, Ba Đình, Hà Nội',0
       , 'Phòng Khám Ba Đình','Fax : N/A', '+84 586037708');

INSERT INTO public."user"(
    id, address, birth, email, experience,flag, identity_card,
    name, note, password, phone_number, sex, specialization, username, branch_id)
VALUES ('NV00001', 'Xã Tân Thới Nhì, Huyện Hóc Môn, Hồ Chí Minh (tphcm)', '2006-02-02', 'hung7nui@gmail.com', '1 năm', 0, '757684756639',
        'Chế Bích Thoa', '','$2a$12$1gdiQMIYtP5wWc8mTSh2Oe619dho5kHPQgjI1pyF.FWsg7vzeJQeS'
           , '0963625458', 'female', 'Quản trị nhân sự', 'thoapro', 'CN00001'),---12345 pass
       ('NV00002', 'Xã Bình Phú, Huyện Tân Hồng, Đồng Tháp', '1972-02-17', 'hunggietgaibangloithitham@gmail.com', '10 năm', 0, '275637485787',
        'Chung Nguyên Lộc','','$2a$12$ZUpdojIOEb1JhToz9x.Y..Tcmv9WzKixiZ9KNENm2ArDaSkCPy7Uu'
           , '0927645355', 'male', 'Thú y', 'loccute', 'CN00001'),---4567 pass
       ('NV00003', 'Thị trấn Tân Túc, Huyện Bình Chánh, Hồ Chí Minh (tphcm)', '1982-11-10','example@gmail.com', '5 năm', 0, '276599987678',
        'Khương Ðình Lộc','','$2a$12$Wtvf1mJ7VcV.3OgMTRQPC.JOhuCOZVDjhxVJ1dIiXcXG./SgRdm9i'
           , '0912347823', 'male', 'Da liễu thú ý', 'chimbay', 'CN00001'),---9876 pass
       ('NV00004', 'Xã Yên Trị, Huyện Yên Thủy, Hoà Bình', '1998-08-28','hungmuaxuan@gmail.com', '1 năm', 0, '087264775898',
        'Danh Cẩm Yến','','$2a$12$olPTryw8MOWP2/81vGtNYeKOkM9Js/5.rr81mT.l3geW1CeoSMUxO'
           , '0927665436', 'female', 'Kinh doanh quốc tế', 'camxinhdep', 'CN00001'),---22222 pass
       ('NV00005', 'Xã Yên Mỹ, Huyện Ý Yên, Nam Định', '2001-07-29','hungnhatho@gmail.com' , 'Không có', 0, '987646277789',
        'Khúc Như Hồng','','$2a$12$YTSOTRWIbszue8hx4oD7RO.OZdeUi39Rrmb0N9O9AQ2ICN7dAmdlO'
           , '0943288987', 'female', 'Kế Toán', 'honghong', 'CN00001');---1234 pass


INSERT INTO public.user_role(
    user_id, role_id)
VALUES ('NV00001', 2),
       ('NV00002', 1),
       ('NV00003', 1),
       ('NV00004', 2),
       ('NV00005', 2);


INSERT INTO public.customer(
    id, address, email,flag, identify_card, name, phone_number, tax_code, type)
VALUES ('KH00001', 'Xã Thắng Lợi, Huyện Văn Giang, Hưng Yên', 'hoathuthuan829@gmail.com', 0, '574885756439'
       , 'Hoa Thu Thuận', '0985647635', '160000', 'normal'),
       ('KH00002', 'Xã Trùng Quán, Huyện Văn Lãng, Lạng Sơn
            ', 'giapthanhthu909@icloud.com',0, '5768475646578'
       , 'Giáp Thanh Thư', '0957564353', '240000', 'vip'),
       ('KH00003', 'Phường Hoàng Văn Thụ, Quận Hồng Bàng, Hải Phòng', 'khuongthaitam64@gmail.com', 0, '988562436433'
       , 'Khương Thái Tâm', '01234567789', '854745', 'normal'),
       ('KH00004', 'Thị trấn Cái Đôi Vàm, Huyện Phú Tân, Cà Mau', 'phungnhahuong730@microsoft.com', 0, '857636434312'
       , 'Phùng Nhã Hương', '09753242112', '778822', 'vip'),
       ('KH00005', 'Xã Long Phước, Huyện Long Hồ, Vĩnh Long', 'lutronghung751@gmail.com', 0, '574885756439'
       , 'Lư Trọng Hùng', '0972546372', '987232', 'normal');

INSERT INTO public.customer_branch(
    customer_id, branch_id)
VALUES ('KH00001', 'CN00001'),
       ('KH00002', 'CN00002'),
       ('KH00003', 'CN00003'),
       ('KH00004', 'CN00004'),
       ('KH00005', 'CN00005');

INSERT INTO public.pet(
    id, birth_certi,flag, name, note, sex, customer_id)
VALUES ('TN00001', 'Pusheen_Zoom_Background_Campfire_2020.jpg',0, 'Miu',
        'Thú dễ khóc', 'male', 'KH00001'),
       ('TN00002', 'cat1.jpg',0, 'Lu',
        'Ỉa nhiều', 'female', 'KH00002'),
       ('TN00003', 'cat2.jpg',0, 'Aika',
        'Thú hay đói bụng', 'male', 'KH00001'),
       ('TN00004', 'cat3.jpg',0, 'Cụp',
        'Thú thích cào cấu', 'male', 'KH00003'),
       ('TN00005', 'cat4.jpg',0, 'Ciu',
        'Không chịu ăn', 'male', 'KH00005'),
       ('TN00006', 'cat5.jpg',0, 'Mule',
        'Thú rất nghịch', 'female', 'KH00004');

-- DROP TABLE public.schedule;
-- CREATE  TABLE public.schedule (
--     id SERIAL PRIMARY KEY,
--     date Date NOT NULL ,
--     time time NOT NULL,
--     symptom varchar NOT NULL,
--     note varchar,
--     status varchar NOT NULL,
--     flag integer,
--     plan_doc_id varchar,
--     real_doc_id varchar,
--     pet_id varchar NOT NULL,
--     user_id varchar NOT NULL,
--     foreign key (pet_id) references public.pet (id),
--     foreign key (user_id) references public."user" (id)
-- );


INSERT INTO public.schedule(
    id, date, flag, note, status, symptom, "time",plan_doc_id, pet_id, user_id)
VALUES
       (1, '2022-06-28',0, 'không có', 'process'
       , 'Chân sưng to, đi lại khó khăn', '04:00 PM','NV00003','TN00002', 'NV00001'),
       (2, '2022-06-10',0, 'không có', 'process'
       , 'Bị nấm móng', '7:00 PM','NV00002', 'TN00003','NV00001'),
       (3, '2022-4-20',0, 'không có', 'success'
       , 'Không kêu trong nhiều ngày', '08:15 AM','NV00003', 'TN00004','NV00001'),
       (4, '2021-10-25',0, 'không có', 'success'
       , 'Bé bị rụng lông', '10:30 PM','NV00003', 'TN00005','NV00001'),
        (5, '2022-05-26',0, 'không có', 'process'
    , 'Chảy nước mắt nhiều', '10:30 AM','NV00003', 'TN00005','NV00001');

SELECT setval('public.schedule_id_seq', (SELECT MAX(id) FROM public.schedule));

INSERT INTO public.type(
    id,flag, name, note)
VALUES ('L00001',0, 'Chó', ''),
       ('L00002',0, 'Mèo', ''),
       ('L00003',0, 'Lợn', ''),
       ('L00004',0, 'Gà', ''),
       ('L00005',0, 'Thỏ', '');

INSERT INTO public.species(
    id,flag, name, note, type_id)
VALUES ('G00001',0, 'Chó Phú Quốc', '', 'L00001'),
       ('G00002',0, 'Chó Corgi', '', 'L00001'),
       ('G00003',0, 'Mèo Anh', '', 'L00002'),
       ('G00004',0, 'Mèo Nga', '', 'L00003'),
       ('G00005',0, 'Lợn mini Bắc Phi', '', 'L00003'),
       ('G00006',0, 'Thỏ Trắng Pháp', '', 'L00005');


INSERT INTO public.pet_species(
    pet_id, species_id)
VALUES ('TN00001', 'G00003'),
       ('TN00002', 'G00003'),
       ('TN00003', 'G00002'),
       ('TN00004', 'G00001'),
       ('TN00005', 'G00006'),
       ('TN00006', 'G00004');


-- INSERT INTO public.user_device(
--     id, flag, token, status, user_id)
-- VALUES (1, 0,'hi', 'ok', 'NV00001');
