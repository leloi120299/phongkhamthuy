package com.backend.Mock3project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@ComponentScan(basePackages = {"com.backend.Mock3project.mapper",
        "com.backend.Mock3project.controller", "com.backend.Mock3project.entity",
        "com.backend.Mock3project.repository", "com.backend.Mock3project.service",
        "com.backend.Mock3project.security","com.backend.Mock3project.exception",
        "com.backend.Mock3project.dto","com.backend.Mock3project.response",
        "com.backend.Mock3project.utils","com.backend.Mock3project.enumeration",
        "com.backend.Mock3project.generator","com.backend.Mock3project.filtering"})
 public class Mock3ProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(Mock3ProjectApplication.class, args);
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
