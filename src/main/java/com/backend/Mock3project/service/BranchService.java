package com.backend.Mock3project.service;

import com.backend.Mock3project.dto.BranchDto;
import com.backend.Mock3project.dto.BranchRequestDto;
import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public interface BranchService {

    /**
     * Get all Branches
     * @int pageNum
     * @size pageSize
     * return List<BranchDto>, total number of Branches
     */
    Page<BranchDto> getBranches(int pageNum, int pageSize);
    /**
     * Get Branches By Id
     * @String id
     * @throws NotFoundException --throw not found
     * @return BranchDto
     */
    BranchDto getBranchById(String id) throws NotFoundException;
    /**
     * add Branches
     * @BranchRequestDto branchRequestDto
     */
    void saveBranch(BranchRequestDto branchRequestDto);
    /**
     * delete Branches by Id
     * @String id
     * @throws NotFoundException --throw not found
     */
    void deleteBranch(String id) throws Exception;
    /**
     * updateBranch Branches by Id
     * @String id
     * @BranchRequestDto branchRequestDto
     * @throws NotFoundException --throw not found
     */
    void updateBranch(String id, BranchRequestDto branchRequestDto) throws NotFoundException;
    /**
     * search all  Branches by name,address,phone,page number,page size
     * @String name
     * @String address
     * @String phone
     * @int pageNum
     * @int pageSize
     * @return List<BranchDto>
     */
    Page<BranchDto> searchBranch(String name, String address, String phone, int pageNum, int pageSize);
}
