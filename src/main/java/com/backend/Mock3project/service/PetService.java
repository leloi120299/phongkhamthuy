package com.backend.Mock3project.service;

import com.backend.Mock3project.dto.PetDto;
import com.backend.Mock3project.dto.PetRequestDto;
import com.backend.Mock3project.entity.Pet;
import org.springframework.data.domain.Page;


public interface PetService {
    /**
     * Get all Pet
     * @int page
     * @size size
     * return List<PetDto>, total number of Pet
     */
    Page<PetDto> getAllPet(int page, int size);

    /**
     * Search pet by id
     * @String id
     * return PetDto
     */
    PetDto searchPetById(String id);

    /**
     * Search Pet
     * @int page
     * @size size
     * @String name
     * return List<PetDto>, total number of Pet
     */
    Page<PetDto> searchPet(int page, int size, String name);

    /**
     * Save pet
     * @PetRequestDto petRequestDto
     * return Pet
     * @throws Exception
     */
    Pet savePet(PetRequestDto petRequestDto) throws Exception;

    /**
     * Update pet
     * @PetRequestDto petRequestDto
     * return Pet
     * @throws Exception
     */
    Pet updatePet(String id, PetRequestDto petRequestDto) throws Exception;

    /**
     * Delete pet
     * @PetRequestDto petRequestDto
     * return void
     * @throws Exception
     */
    void deletePet(String id) throws Exception;
}
