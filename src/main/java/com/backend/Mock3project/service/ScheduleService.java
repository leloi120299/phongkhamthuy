package com.backend.Mock3project.service;

import com.backend.Mock3project.dto.ScheduleDto;
import com.backend.Mock3project.dto.ScheduleRequestDto;
import com.backend.Mock3project.entity.Schedule;
import javassist.NotFoundException;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

public interface ScheduleService {
    /**
     * Get all Schedule
     * @int page
     * @int size
     * @return List<ScheduleDto>
     */
    List<ScheduleDto> getAllSchedule(int page, int size);

    /**
     * Count schedule
     * @return Integer
     */
    Integer countSchedules();

    /**
     * Search schedule by name of customer
     * @int page
     * @int size
     * @String name
     * @return List<ScheduleDto>
     */
    List<ScheduleDto> searchScheduleByNameCustomer(int page, int size, String name);

    /**
     * Count schedule by name customer
     * @String name
     * @return Integer
     */
    Integer countScheduleByNameCustomer(String name);

    /**
     * Search schedule by id
     * @Integer id
     * @return ScheduleDto
     */
    ScheduleDto findScheduleById(Integer id) throws NotFoundException;

    /**
     * Search schedule by date and time
     * @int page
     * @int size
     * @LocalDate date
     * @LocalTime time
     * @return List<ScheduleDto>
     */
    List<ScheduleDto> findByDateAndTime(int page, int size, LocalDate date, LocalTime time);

    /**
     * Count by date and time
     * @LocalDate date
     * @LocalTime time
     * @return Integer
     */
    Integer countByDateAndTime(LocalDate date, LocalTime time);

    /**
     * Search schedule by branch id
     * @int page
     * @int size
     * @String id
     * @return List<ScheduleDto>
     */
    List<ScheduleDto> findSchedulesByBranchId(int page, int size, String id);

    /**
     * Count schedule by branch id
     * @String id
     * @return Integer
     */
    Integer countSchedulesByBranchId(String id);

    /**
     * Search schedule by doc id
     * @int page
     * @int size
     * @String id
     * @return List<ScheduleDto>
     */
    List<ScheduleDto> findSchedulesByDocId(int page, int size, String id);

    /**
     * Count schedule by doc id
     * @String id
     * @return Integer
     */
    Integer countSchedulesByDocId(String id);

    /**
     * Search schedule by interval date
     * @int page
     * @int size
     * @Date date1
     * @Date date2
     * @return List<ScheduleDto>
     */
    List<ScheduleDto> findSchedulesByDateInterval(int page, int size, Date date1, Date date2);

    /**
     * Count schedule by interval date
     * @Date date1
     * @Date date2
     * @return Integer
     */
    Integer countSchedulesByDateInterval(Date date1, Date date2);

    /**
     * Count schedule by status
     * @String status
     * @return Integer
     */
    public Integer countScheduleByStatus(String status) throws NotFoundException;

    /**
     * Save schedule
     * @ScheduleRequestDto scheduleRequestDto
     * @return Schedule
     */
    Schedule saveSchedule(ScheduleRequestDto scheduleRequestDto);

    /**
     * Delete schedule by id
     * @int id
     * @throws NotFoundException
     */
    void deleteScheduleById(int id) throws NotFoundException;

    /**
     * Update schedule
     * @int id
     * @ScheduleRequestDto scheduleRequestDto
     * @return Schedule
     * @throws NotFoundException
     */
    Schedule updateSchedule(int id, ScheduleRequestDto scheduleRequestDto) throws NotFoundException;
}
