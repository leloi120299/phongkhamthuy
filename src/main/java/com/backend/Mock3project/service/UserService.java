package com.backend.Mock3project.service;


import com.backend.Mock3project.dto.UserDto;
import com.backend.Mock3project.dto.UserRequestDto;
import com.backend.Mock3project.entity.User;
import org.springframework.data.domain.Page;


public interface UserService {

    /**
     * Save User
     * @UserRequestDto userRequestDto
     * return User
     * @throws Exception
     */
    User saveUser(UserRequestDto userRequestDto) throws Exception;

    /**
     * Get User by username
     * @String username
     * return User
     */
    User getUser(String username);

    /**
     * Change User password
     * @String username
     * @String oldPassword
     * @String newPassword
     * return void
     * @throws Exception
     */
    void changePassword(String username, String oldPassword, String newPassword) throws Exception;

    /**
     * Search User by id
     * @String id
     * return UserDto
     * @throws Exception
     */
    UserDto searchById(String id) throws Exception;

    /**
     * Forgot User password
     * @String username
     * @String newPassword
     * @String gmail
     * return void
     * @throws Exception
     */
    void forgotPassword(String username, String newPassword, String gmail) throws Exception;

    /**
     * Search User
     * @int page
     * @size size
     * @String name
     * @String phoneNumber
     * @String role
     * @String branch
     * return Page<UserDto>, total number of Pet
     */
    Page<UserDto> searchUser(int page, int size, String name, String phoneNumber, String role, String branch);

    /**
     * Get all User
     * @int page
     * @int size
     * return Page<UserDto>, total number of Pet
     */
    Page<UserDto> getAllUser(int page, int size);

    /**
     * Update User
     * @String id
     * @UserRequestDto petRequestDto
     * return User
     * @throws Exception
     */
    User updateUser(String id, UserRequestDto userRequestDto) throws Exception;

    /**
     * Delete User
     * @String id
     * return void
     * @throws Exception
     */
    void deleteUser(String id) throws Exception;
}
