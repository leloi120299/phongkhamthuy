package com.backend.Mock3project.service;

import com.backend.Mock3project.dto.CustomerDto;
import com.backend.Mock3project.dto.CustomerRequestDto;
import com.backend.Mock3project.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;


@Service
public interface CustomerService {
    /**
     * Get all Customer
     * @int page
     * @size size
     * return Page<CustomerDto>, total number of Customer
     */
    Page<CustomerDto> getAllCustomer(int page, int size);

    /**
     * Search Customer
     * @int page
     * @size size
     * @String name
     * @String branch
     * return Page<CustomerDto>, total number of Customer
     */
    Page<CustomerDto> searchCustomer(int page, int size, String name, String branch);

    /**
     * Search customer by id
     * @String id
     * return CustomerDto
     */
    CustomerDto searchCustomerById(String id);

    /**
     * Save customer
     * @CustomerRequestDto customerRequestDto
     * return Customer
     */
    Customer saveCustomer(CustomerRequestDto customerRequestDto);

    /**
     * Update customer
     * @String id
     * @CustomerRequestDto customerRequestDto
     * return Customer
     */
    Customer updateCustomer(String id, CustomerRequestDto customerRequestDto);

    /**
     * Delete customer
     * @String id
     * return void
     */
    void deleteCustomer(String id) throws Exception;

}
