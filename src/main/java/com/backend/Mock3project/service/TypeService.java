package com.backend.Mock3project.service;

import com.backend.Mock3project.dto.TypeDto;
import com.backend.Mock3project.dto.TypeRequestDto;
import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public interface TypeService {

    /**
     * Get all type
     * @int pageNum
     * @int pageSize
     * @return List<TypeDto>, total number of type
     */
    Page<TypeDto> getListType(int pageNum, int pageSize);

    /**
     * Search Type by name
     * @String name
     * @int pageNum
     * @int pageSize
     * return List<TypeDto>, total number of Type
     */
    Page<TypeDto> searchByTypeName(String name, int pageNum, int pageSize);

    /**
     * Search Type by id
     * @String id
     * return typeDto
     */
    TypeDto searchByTypeId(String id) throws NotFoundException;;

    /**
    *  Add new Type
    *  @TypeRequestDto typeRequestDto
    */
    void saveType(TypeRequestDto typeRequestDto) throws Exception;

    /**
     *  update type
     *  @String id
     *  @TypeRequestDto typeRequestDto
     */
    void updateType(String id, TypeRequestDto typeRequestDto) throws Exception;

    /**
    *  delete type
    *  @String id
    */
    void deleteType(String id) throws Exception;
}
