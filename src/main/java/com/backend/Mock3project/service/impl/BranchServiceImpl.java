package com.backend.Mock3project.service.impl;

import com.backend.Mock3project.dto.BranchDto;
import com.backend.Mock3project.dto.BranchRequestDto;
import com.backend.Mock3project.mapper.BranchMapper;
import com.backend.Mock3project.repository.BranchRepository;
import com.backend.Mock3project.repository.CustomerRepository;
import com.backend.Mock3project.repository.PetRepository;
import com.backend.Mock3project.service.*;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
@Service
@Transactional
public class BranchServiceImpl implements BranchService {

    @Autowired
    BranchRepository branchRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    TypeService typeService;

    @Autowired
    SpeciesService speciesService;

    @Autowired
    PetRepository petRepository;
    @Autowired
    private BranchMapper branchMapper;


    @Override
    public Page<BranchDto> getBranches(int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize);
        return branchMapper.pageBranchToListBranchDTO(branchRepository.findAllByFlagEquals(0, pageable));
    }

    @Override
    public BranchDto getBranchById(String id) throws NotFoundException {
        if (!branchRepository.findById(id).isPresent()) {
            throw new NotFoundException("Branch with id = " + id + " not exist");
        }
        return branchMapper.branchToBranchDTO(branchRepository.findById(id).get());
    }

    @Override
    public void saveBranch(BranchRequestDto branchRequestDto) {
        branchRepository.save(branchMapper.branchRequestDTOToBranch(branchRequestDto));
    }

    @Override
    public void deleteBranch(String id) throws Exception {
        if (!branchRepository.findById(id).isPresent()) {
            throw new NotFoundException("Branch with id = " + id + " not exist");
        }
        if (branchRepository.deleteBranch(id) == null) {
            throw new Exception("Customer or user related branch with id = " + id + " exist ,can not delete the branch");
        }
    }

    @Override
    public void updateBranch(String id, BranchRequestDto branchRequestDto) throws NotFoundException {
        if (!branchRepository.findById(id).isPresent()) {
            throw new NotFoundException("Branch with id = " + id + " not exist");
        }
        branchRequestDto.setId(id);
        branchRepository.save(branchMapper.branchRequestDTOToBranch(branchRequestDto));
    }

    @Override
    public Page<BranchDto> searchBranch(String name, String address, String phone, int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize);
        return branchMapper.pageBranchToListBranchDTO(
                branchRepository.searchAllBranch
                        (name, address, phone, pageable));
    }

}
