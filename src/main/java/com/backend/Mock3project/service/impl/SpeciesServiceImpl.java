package com.backend.Mock3project.service.impl;

import com.backend.Mock3project.dto.SpeciesDto;
import com.backend.Mock3project.dto.SpeciesRequestDto;
import com.backend.Mock3project.entity.Type;
import com.backend.Mock3project.mapper.SpeciesMapper;
import com.backend.Mock3project.repository.SpeciesRepository;
import com.backend.Mock3project.repository.TypeRepository;
import com.backend.Mock3project.service.SpeciesService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class SpeciesServiceImpl implements SpeciesService {

    @Autowired
    SpeciesRepository speciesRepository;
    @Autowired
    TypeRepository typeRepository;

    @Autowired
    SpeciesMapper speciesMapper;

    @Override
    public Page<SpeciesDto> getSpecies(int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize);
        return speciesMapper.pageSpeciesToListSpeciesDTO(
                speciesRepository.findAllByFlagEquals(0, pageable));
    }

    @Override
    public void saveSpecies(SpeciesRequestDto speciesRequestDto) throws NotFoundException {
        Optional<Type> type = typeRepository.findById(speciesRequestDto.getTypeId());
        if (!type.isPresent()) {
            throw new NotFoundException("Type with id = " + speciesRequestDto.getTypeId() + " not exist");
        }
        speciesRequestDto.setType(type.get());
        speciesRepository.save(speciesMapper.speciesRequestDTOToSpecies(speciesRequestDto));

    }

    @Override
    public void deleteSpecies(String id) throws Exception {
        if (!speciesRepository.findById(id).isPresent()) {
            throw new NotFoundException("Species with id = " + id + " not exist");
        }
        if (speciesRepository.deleteSpecies(id) == null) {
            throw new Exception("Pet related species with id = " + id + " exist ,can not delete the species");
        }
    }

    @Override
    public void updateSpecies(String id, SpeciesRequestDto speciesRequestDto) throws NotFoundException {
        Optional<Type> type = typeRepository.findById(speciesRequestDto.getTypeId());
        if (!speciesRepository.findById(id).isPresent()) {
            throw new NotFoundException("Species with id = " + id + " not exist");
        } else if (!type.isPresent()) {
            throw new NotFoundException("Type with id = " + id + " not exist");
        }
        speciesRequestDto.setId(id);
        speciesRequestDto.setType(type.get());
        speciesRepository.save(speciesMapper.speciesRequestDTOToSpecies(speciesRequestDto));
    }

    @Override
    public Page<SpeciesDto> searchBySpeciesName(String name, int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize);
        return speciesMapper.pageSpeciesToListSpeciesDTO(
                speciesRepository.findAllByNameIsContainingIgnoreCase(name, pageable));
    }

    @Override
    public SpeciesDto getSpeciesById(String id) throws NotFoundException {
        if (!speciesRepository.findById(id).isPresent()) {
            throw new NotFoundException("Species with id = " + id + " not exist");
        }
        return speciesMapper.speciesToSpeciesDTO(speciesRepository.findById(id).get());
    }

}
