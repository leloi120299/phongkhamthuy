package com.backend.Mock3project.service.impl;

import com.backend.Mock3project.dto.ScheduleAdditionalDataDto;
import com.backend.Mock3project.dto.ScheduleDto;
import com.backend.Mock3project.dto.ScheduleRequestDto;
import com.backend.Mock3project.entity.Schedule;
import com.backend.Mock3project.mapper.ScheduleMapper;
import com.backend.Mock3project.repository.ScheduleRepository;
import com.backend.Mock3project.repository.UserRepository;
import com.backend.Mock3project.service.ScheduleService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ScheduleServiceImpl implements ScheduleService {
    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private ScheduleMapper scheduleMapper;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<ScheduleDto> getAllSchedule(int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        List<ScheduleDto> scheduleDtos = new ArrayList<>();
        List<Schedule> schedules = scheduleRepository.getAllSchedule(pageable).getContent();
        return getScheduleDtos(scheduleDtos, schedules);
    }

    @Override
    public Integer countSchedules(){
        return scheduleRepository.countSchedules();
    }

    @Override
    public List<ScheduleDto> searchScheduleByNameCustomer(int page, int size, String name) {
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        List<ScheduleDto> scheduleDtos = new ArrayList<>();
        List<Schedule> schedules = scheduleRepository.searchScheduleByNameCustomer(name, pageable).getContent();
        return getScheduleDtos(scheduleDtos, schedules);
    }

    @Override
    public Integer countScheduleByNameCustomer(String name) {
        return scheduleRepository.countScheduleByNameCustomers(name);
    }

    @Override
    public ScheduleDto findScheduleById(Integer id) throws NotFoundException {
        if (!scheduleRepository.findById(id).isPresent()) {
            throw new NotFoundException("Schedule with id = " + id + " not exists");
        }
        Schedule schedule = scheduleRepository.findById(id).get();
        ScheduleDto scheduleDto = scheduleMapper.mapEntityToDto(schedule);
        List<Object[]> detailObject = scheduleRepository.getScheduleDetailById(id);
        scheduleDto.setAdditionalDetail(new ScheduleAdditionalDataDto((String) detailObject.get(0)[0],
                (String) detailObject.get(0)[1], (String) detailObject.get(0)[2], (String) detailObject.get(0)[3],
                (String) detailObject.get(0)[4], (String) detailObject.get(0)[5], (String) detailObject.get(0)[6],
                (String) detailObject.get(0)[7], (String) detailObject.get(0)[8], (String) detailObject.get(0)[9],
                (String) detailObject.get(0)[10]));
        return scheduleDto;
    }

    @Override
    public List<ScheduleDto> findByDateAndTime(int page, int size, LocalDate date, LocalTime time) {
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        List<ScheduleDto> scheduleDtos = new ArrayList<>();
        List<Schedule> schedules = scheduleRepository.findByDateAndTime(date, time, pageable).getContent();
        return getScheduleDtos(scheduleDtos, schedules);
    }

    @Override
    public Integer countByDateAndTime(LocalDate date, LocalTime time) {
        return scheduleRepository.countByDateAndTime(date,time);
    }

    @Override
    public List<ScheduleDto> findSchedulesByBranchId(int page, int size, String id) {
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        List<ScheduleDto> scheduleDtos = new ArrayList<>();
        List<Schedule> schedules = scheduleRepository.findSchedulesByBranchId(id, pageable).getContent();
        return getScheduleDtos(scheduleDtos, schedules);
    }

    @Override
    public Integer countSchedulesByBranchId(String id) {
        return scheduleRepository.countSchedulesByBranchId(id);
    }

    @Override
    public List<ScheduleDto> findSchedulesByDocId(int page, int size, String id) {
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        List<ScheduleDto> scheduleDtos = new ArrayList<>();
        List<Schedule> schedules = scheduleRepository.findSchedulesByDocId(id, pageable).getContent();
        return getScheduleDtos(scheduleDtos, schedules);
    }

    @Override
    public Integer countSchedulesByDocId(String id) {
        return scheduleRepository.countSchedulesByDocId(id);
    }

    @Override
    public List<ScheduleDto> findSchedulesByDateInterval(int page, int size, Date date1, Date date2) {
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        List<ScheduleDto> scheduleDtos = new ArrayList<>();
        List<Schedule> schedules = scheduleRepository.findSchedulesByDateInterval(date1, date2, pageable).getContent();
        return getScheduleDtos(scheduleDtos, schedules);
    }

    @Override
    public Integer countSchedulesByDateInterval(Date date1, Date date2) {
        return scheduleRepository.countSchedulesByDateInterval(date1, date2);
    }

    private List<ScheduleDto> getScheduleDtos(List<ScheduleDto> scheduleDtos, List<Schedule> schedules) {
        int bound = schedules.toArray().length;
        List<Integer> idSchedule = new ArrayList<>();
        for (Schedule schedule : schedules) {
            idSchedule.add(schedule.getId());
        }
        List<Object[]> listScheduleDetail = scheduleRepository.getScheduleDetail(idSchedule);
        for (int i = 0; i < bound; i++) {
            ScheduleDto scheduleDto = scheduleMapper.mapEntityToDto(schedules.get(i));
            scheduleDto.setAdditionalDetail(new ScheduleAdditionalDataDto((String) listScheduleDetail.get(i)[0],
                    (String) listScheduleDetail.get(i)[1], (String) listScheduleDetail.get(i)[2], (String) listScheduleDetail.get(i)[3],
                    (String) listScheduleDetail.get(i)[4], (String) listScheduleDetail.get(i)[5], (String) listScheduleDetail.get(i)[6],
                    (String) listScheduleDetail.get(i)[7], (String) listScheduleDetail.get(i)[8], (String) listScheduleDetail.get(i)[9],
                    (String) listScheduleDetail.get(i)[10]));
            scheduleDtos.add(scheduleDto);
        }
        return scheduleDtos;
    }

    @Override
    public Integer countScheduleByStatus(String status) throws NotFoundException {
        if (scheduleRepository.findByStatus(status).size() == 0) {
            throw new NotFoundException("Schedule status like " + status + " not exist");
        }
        return scheduleRepository.countAllByStatus(status);
    }

    @Override
    public Schedule saveSchedule(ScheduleRequestDto scheduleRequestDto) {
        String idPlanDoc = scheduleRequestDto.getPlanDocId();
        String idRealDoc = scheduleRequestDto.getRealDocId();
        if (idPlanDoc != null && userRepository.getDocById(idPlanDoc) == null || idRealDoc != null && userRepository.getDocById(idRealDoc) == null) {
            return null;
        }
        return scheduleRepository.save(scheduleMapper.mapDtoToEntity(scheduleRequestDto));
    }

    @Override
    public void deleteScheduleById(int id) throws NotFoundException {
        if (!scheduleRepository.findById(id).isPresent()) {
            throw new NotFoundException("Schedule id = " + id + " not exist");
        }
        scheduleRepository.deleteScheduleById(id);
    }

    @Override
    public Schedule updateSchedule(int id, ScheduleRequestDto scheduleRequestDto) throws NotFoundException {
        if (!scheduleRepository.findById(id).isPresent()) {
            throw new NotFoundException("Schedule id = " + id + " not exist");
        }
        String idPlanDoc = scheduleRequestDto.getPlanDocId();
        String idRealDoc = scheduleRequestDto.getRealDocId();
        if (idPlanDoc != null && userRepository.getDocById(idPlanDoc) == null || idRealDoc != null && userRepository.getDocById(idRealDoc) == null) {
            return null;
        }
        scheduleRequestDto.setId(id);
        return scheduleRepository.save(scheduleMapper.mapDtoToEntity(scheduleRequestDto));
    }
}
