package com.backend.Mock3project.service.impl;

import com.backend.Mock3project.dto.TypeDto;
import com.backend.Mock3project.dto.TypeRequestDto;
import com.backend.Mock3project.entity.Type;
import com.backend.Mock3project.mapper.TypeMapper;
import com.backend.Mock3project.repository.SpeciesRepository;
import com.backend.Mock3project.repository.TypeRepository;
import com.backend.Mock3project.service.TypeService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.function.Function;


@Service
@Transactional
public class TypeServiceImpl implements TypeService {

    @Autowired
    SpeciesRepository speciesRepository;

    @Autowired
    TypeRepository typeRepository;

    @Autowired
    TypeMapper typeMapper;

    @Override
    public Page<TypeDto> getListType(int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize);
        Page<Type> entities  = typeRepository.getListType(pageable);
        return entities.map(new Function<Type, TypeDto>() {
            @Override
            public TypeDto apply(Type type) {
                return typeMapper.typeToTypeDTO(type);
            }
        });
    }

    @Override
    public Page<TypeDto> searchByTypeName(String name, int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize);
        Page<Type> entities = typeRepository.searchByName(name, pageable);
        return entities.map(new Function<Type, TypeDto>() {
            @Override
            public TypeDto apply(Type type) {
                return typeMapper.typeToTypeDTO(type);
            }
        });
    }

    @Override
    public TypeDto searchByTypeId(String id) throws NotFoundException {
        if(!typeRepository.getTypeById(id).isPresent()){
            throw new NotFoundException("Type with id = "+ id +" not exists");
        }
        return typeMapper.typeToTypeDTO(typeRepository.getTypeById(id).get());

    }

    @Override
    public void saveType(TypeRequestDto typeRequestDto) throws Exception{
        Pageable pageable = null;
        if (typeRepository.searchByName(typeRequestDto.getName(), pageable).getTotalElements() != 0) {
            throw new Exception("Type with name = "+ typeRequestDto.getName() +" exists");
        }
        typeRepository.save(typeMapper.typeRequestDTOToType(typeRequestDto));

    }

    @Override
    public void updateType(String id, TypeRequestDto typeRequestDto) throws Exception {
        Pageable pageable = null;
        if (!typeRepository.getTypeById(id).isPresent()) {
            throw new Exception("Type with id = " + id + " not exist");
        } else if (typeRequestDto.getName().equals(typeRepository.getNameById(id))) {
            typeRequestDto.setId(id);
            typeRepository.save(typeMapper.typeRequestDTOToType(typeRequestDto));
        } else if (typeRepository.searchByName(typeRequestDto.getName(), pageable).getTotalElements() == 0) {
            typeRequestDto.setId(id);
            typeRepository.save(typeMapper.typeRequestDTOToType(typeRequestDto));
        } else {
            throw new Exception("Type with name = "+ typeRequestDto.getName() +" exists");
        }

    }

    @Override
    public void deleteType(String id) throws Exception {
        if (!typeRepository.getTypeById(id).isPresent()) {
            throw new NotFoundException("Type with id = " + id + " not exist");
        } else if (speciesRepository.searchByTypeId(id).size() != 0){
            throw new Exception("Species still exist with type_id = " + id );
        }
        typeRepository.getTypeById(id).get().setFlag(1);

    }
}
