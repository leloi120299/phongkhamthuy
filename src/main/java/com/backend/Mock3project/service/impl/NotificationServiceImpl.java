package com.backend.Mock3project.service.impl;

import com.backend.Mock3project.dto.NotificationRequest;
import com.backend.Mock3project.dto.UserDeviceRequestDto;
import com.backend.Mock3project.dto.UserNotificationDto;
import com.backend.Mock3project.entity.User;
import com.backend.Mock3project.entity.UserDevice;
import com.backend.Mock3project.entity.UserNotification;
import com.backend.Mock3project.mapper.UserDeviceMapper;
import com.backend.Mock3project.repository.UserDeviceRepository;
import com.backend.Mock3project.repository.UserNotificationRepository;
import com.backend.Mock3project.repository.UserRepository;
import com.backend.Mock3project.service.FCMService;
import com.backend.Mock3project.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
@DependsOn("FCMInitializer")
public class NotificationServiceImpl implements NotificationService {
    @Autowired
    private FCMService fcmService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDeviceRepository userDeviceRepository;

    @Autowired
    private UserNotificationRepository userNotificationRepository;

    @Autowired
    private UserDeviceMapper userDeviceMapper;

    private User userRender(String userId) {
        User user = new User();
        user.setId(userId);
        return user;
    }

    @Override
    public void sendNotificationToToken(String userId, NotificationRequest request) throws Exception {
        List<String> tokenList = userDeviceRepository.getTokenByUser(userId);
        if (tokenList == null) {
            throw new Exception("Token is not exist");
        }
        boolean isFirst = true;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        for (String s : tokenList) {
            request.setToken(s);
            try {
                fcmService.sendMessageToToken(request);
                if (isFirst) {
                    UserNotification userNotification = userDeviceMapper.userNotificationRequestToUserNotification(request);
                    userNotification.setFlag(0);
                    userNotification.setUser(userRender(userId));
                    userNotification.setDate(formatter.parse(request.getDateString()));
                    userNotificationRepository.save(userNotification);
                    isFirst = false;
                }
            } catch (Exception e) {
                throw new Exception(e);
            }
        }
    }

    @Override
    public void addUserDevice(UserDeviceRequestDto request) throws Exception{
        String userId = request.getUserId();
        if (userRepository.getUserById(userId) == null) {
            throw new Exception("User is not exist");
        }
        UserDevice userDevice = userDeviceMapper.userDeviceRequestToUserDevice(request);
        String token = userDevice.getToken();
        if (userDeviceRepository.getUserByToken(token) == null) {
            userDevice.setFlag(0);
            userDevice.setStatus("login");
            userDeviceRepository.save(userDevice);
        }
        userDeviceRepository.loginUser(userId, token);
    }

    @Override
    public Integer deactivateUserDevice(String token) {
        return userDeviceRepository.logoutUser(token);
    }

    @Override
    public Page<UserNotificationDto> getAllNotificationByUser(int page, int size, String userId) {
        if (userRepository.getUserById(userId) == null) {
            return null;
        }
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        Page<UserNotification> entities = userNotificationRepository.getAllUserNotification(userId, pageable);
        System.out.println(entities);
        return entities.map(new Function<UserNotification, UserNotificationDto>() {
            @Override
            public UserNotificationDto apply(UserNotification userNotification) {
                return userDeviceMapper.userNotificationToUserNotificationDto(userNotification);
            }
        });
    }
}
