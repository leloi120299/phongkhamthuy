package com.backend.Mock3project.service.impl;


import com.backend.Mock3project.dto.UserDto;
import com.backend.Mock3project.dto.UserRequestDto;
import com.backend.Mock3project.entity.Role;
import com.backend.Mock3project.entity.User;
import com.backend.Mock3project.enumeration.EmployeeType;
import com.backend.Mock3project.mapper.UserMapper;
import com.backend.Mock3project.repository.RoleRepository;
import com.backend.Mock3project.repository.UserRepository;
import com.backend.Mock3project.security.UserDetailsImpl;
import com.backend.Mock3project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@Service
@Transactional
public class UserServiceImpl implements UserService, UserDetailsService {
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserMapper userMapper;

    @Override
    public Page<UserDto> getAllUser(int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        Page<User> entities = userRepository.getAllUser(pageable);
        return entities.map(new Function<User, UserDto>() {
            @Override
            public UserDto apply(User user) {
                return userMapper.userToUserDto(user);
            }
        });
    }

    @Override
    public Page<UserDto> searchUser(int page, int size, String name, String phoneNumber, String role, String branch) {
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        Page<User> entities = userRepository.searchAllUser(name, phoneNumber, role, branch, pageable);
        return entities.map(new Function<User, UserDto>() {
            @Override
            public UserDto apply(User user) {
                return userMapper.userToUserDto(user);
            }
        });
    }

    @Override
    public User getUser(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Override
    public void changePassword(String username, String oldPassword, String newPassword) throws Exception {
        User user = userRepository.getUserByUsername(username);
        if (user == null) {
            throw new Exception("User is not exist");
        }
        if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
            throw new Exception("Not right password");
        }
        userRepository.changePasswordByUsername(username, passwordEncoder.encode(newPassword));
    }

    @Override
    public UserDto searchById(String id) throws Exception {
        User user = userRepository.getUserById(id);
        if (user == null) {
            throw new Exception("User not found");
        }
        return userMapper.userToUserDto(user);
    }

    @Override
    public void forgotPassword(String username, String newPassword, String email) throws Exception {
        User user = userRepository.getUserByUsername(username);
        if (user == null) {
            throw new Exception("User is not exist");
        }
        if (!user.getEmail().equals(email)) {
            throw new Exception("Not right email");
        }
        userRepository.forgotPasswordByUsername(username, passwordEncoder.encode(newPassword), email);
    }

    @Override
    public User saveUser(UserRequestDto userRequestDto) throws Exception {
        userRequestDto.setPassword(passwordEncoder.encode(userRequestDto.getPassword()));
        User user = userMapper.userRequestToUser(userRequestDto);
        return mappingRole(userRequestDto, user, true);
    }

    @Override
    public User updateUser(String id, UserRequestDto userRequestDto) throws Exception {
        User userRes = userRepository.getUserById(id);
        if (userRes == null) {
            throw new Exception("User is not exist");
        }
        userRequestDto.setPassword(passwordEncoder.encode(userRequestDto.getPassword()));
        User user = userMapper.userRequestToUser(userRequestDto);
        user.setId(id);
        user.setUsername(userRes.getUsername());
        return mappingRole(userRequestDto, user, false);
    }

    private User mappingRole(UserRequestDto userRequestDto, User user, boolean isAdd) throws Exception {
        if (userRepository.getUserByUsername(userRequestDto.getUsername()) != null && isAdd) {
            throw new Exception("User name is exist");
        }
        List<Role> roles = new ArrayList<>();
        userRequestDto.getRoles().forEach(roleCode -> {
            String name = EmployeeType.getTextByCode(roleCode);
            Role role = roleRepository.findByName(name);
            if (role == null) {
                role = new Role();
            }
            role.setName(name);
            roles.add(role);
        });
        user.setRoles(roles);
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(String id) throws Exception {
        if (userRepository.getUserById(id) == null) {
            throw new Exception("User is not exist");
        }
        if (userRepository.checkExistSchedule(id) == null ) {
            throw new Exception("User is exist in schedule");
        }
        userRepository.deleteUserById(id);
        userRepository.deleteUserDeviceByUser(id);
        userRepository.deleteUserNotificationByUser(id);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (userRepository.getUserByUsername(username) == null) {
            throw new UsernameNotFoundException("User Not Found with username: " + username);
        }
        User user = userRepository.getUserByUsername(username);
        return UserDetailsImpl.build(user);
    }
}
