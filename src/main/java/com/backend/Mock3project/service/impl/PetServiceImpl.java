package com.backend.Mock3project.service.impl;

import com.backend.Mock3project.dto.PetDto;
import com.backend.Mock3project.dto.PetRequestDto;
import com.backend.Mock3project.entity.Pet;
import com.backend.Mock3project.entity.Species;
import com.backend.Mock3project.mapper.PetMapper;
import com.backend.Mock3project.repository.CustomerRepository;
import com.backend.Mock3project.repository.PetRepository;
import com.backend.Mock3project.repository.SpeciesRepository;
import com.backend.Mock3project.service.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Service
@Transactional
public class PetServiceImpl implements PetService {
    @Autowired
    PetRepository petRepository;

    @Autowired
    SpeciesRepository speciesRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    private PetMapper petMapper;

    @Override
    public Page<PetDto> getAllPet(int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        Page<Pet> entities = petRepository.getAllPet(pageable);
        return entities.map(new Function<Pet, PetDto>() {
            @Override
            public PetDto apply(Pet pet) {
                return petMapper.petToPetDto(pet);
            }
        });
    }

    @Override
    public PetDto searchPetById(String id) {
        Pet pet = petRepository.getPetById(id);
        if (pet == null) {
            return null;
        }
        return petMapper.petToPetDto(pet);
    }

    @Override
    public Page<PetDto> searchPet(int page, int size, String name) {
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        Page<Pet> entities = petRepository.searchAllPet(name, pageable);
        return entities.map(new Function<Pet, PetDto>() {
            @Override
            public PetDto apply(Pet pet) {
                return petMapper.petToPetDto(pet);
            }
        });
    }

    @Override
    public Pet savePet(PetRequestDto petRequestDto) throws Exception {
        String customerId = petRequestDto.getCustomerId();
        if (customerId != null && customerRepository.getCustomerById(customerId) == null) {
            throw new Exception("Customer is not exist");
        }
        Pet pet = petMapper.petRequestToPet(petRequestDto);
        return mappingSpecies(petRequestDto, pet);
    }

    @Override
    public Pet updatePet(String id, PetRequestDto petRequestDto) throws Exception {
        Pet petRes = petRepository.getPetById(id);
        if (petRes == null) {
            throw new Exception("Pet is not exist");
        }
        String customerId = petRequestDto.getCustomerId();
        if (customerId != null && customerRepository.getCustomerById(customerId) == null) {
            throw new Exception("Customer is not exist");
        }
        Pet pet = petMapper.petRequestToPet(petRequestDto);
        pet.setId(id);
        return mappingSpecies(petRequestDto, pet);
    }

    private Pet mappingSpecies(PetRequestDto petRequestDto, Pet pet) throws Exception {
        List<Species> species = new ArrayList<>();
        List<String> arrSpecies = Arrays.asList(petRequestDto.getSpecies().split(" "));
        arrSpecies.forEach(speciesCode -> {
            Optional<Species> speciesSingle = speciesRepository.findById(speciesCode);
            speciesSingle.ifPresent(species::add);
        });
        pet.setSpecies(species);
        String uploadRootPath = "/home/vss.backend/mock3img";
        File uploadRootDir = new File(uploadRootPath);
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }
        MultipartFile fileData = petRequestDto.getBirthCerti();
        String name = fileData.getOriginalFilename();
        if (name != null && name.length() > 0) {
            try {
                String path = uploadRootDir.getAbsolutePath() + File.separator +  name;
                File serverFile = new File(path);
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(fileData.getBytes());
                stream.close();
                pet.setBirthCerti(name);
            } catch (Exception e) {
                throw new Exception(e);
            }
        }
        return petRepository.save(pet);
    }

    @Override
    public void deletePet(String id) throws Exception {
        if (petRepository.getPetById(id) == null) {
            throw new Exception("Pet is not exist");
        }
        if (petRepository.checkExistSchedule(id) != null) {
            throw new Exception("Pet is exist in schedule");
        }
        petRepository.deletePetById(id);
    }
}
