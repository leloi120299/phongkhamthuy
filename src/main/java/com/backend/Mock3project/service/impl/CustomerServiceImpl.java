package com.backend.Mock3project.service.impl;

import com.backend.Mock3project.dto.CustomerDto;
import com.backend.Mock3project.dto.CustomerRequestDto;
import com.backend.Mock3project.entity.Branch;
import com.backend.Mock3project.entity.Customer;
import com.backend.Mock3project.mapper.CustomerMapper;
import com.backend.Mock3project.repository.BranchRepository;
import com.backend.Mock3project.repository.CustomerRepository;
import com.backend.Mock3project.repository.PetRepository;
import com.backend.Mock3project.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    BranchRepository branchRepository;

    @Autowired
    PetRepository petRepository;

    @Autowired
    CustomerMapper customerMapper;

    @Override
    public Page<CustomerDto> getAllCustomer(int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        Page<Customer> entities = customerRepository.getAllCustomer(pageable);
        return entities.map(new Function<Customer, CustomerDto>() {
            @Override
            public CustomerDto apply(Customer customer) {
                return customerMapper.customerToCustomerDto(customer);
            }
        });
    }

    @Override
    public Page<CustomerDto> searchCustomer(int page, int size, String name, String branch) {
        Pageable pageable = PageRequest.of(page - 1, Math.max(size, 1));
        Page<Customer> entities = customerRepository.searchAllCustomer(name, branch, pageable);
        return entities.map(new Function<Customer, CustomerDto>() {
            @Override
            public CustomerDto apply(Customer customer) {
                return customerMapper.customerToCustomerDto(customer);
            }
        });
    }

    @Override
    public CustomerDto searchCustomerById(String id) {
        Customer customer = customerRepository.getCustomerById(id);
        if(customer == null){
            return null;
        }
        return customerMapper.customerToCustomerDto(customer);
    }

    @Override
    public Customer saveCustomer(CustomerRequestDto customerRequestDto) {
        Customer customer = customerMapper.customerRequestToCustomer(customerRequestDto);
        List<Branch> branches = new ArrayList<>();
        customerRequestDto.getBranches().forEach(branchesCode -> {
            Optional<Branch> branchSingle = branchRepository.findById(branchesCode);
            branchSingle.ifPresent(branches::add);
        });
        customer.setBranches(branches);
        Customer customerSave = customerRepository.save(customer);
        List<String> idPet = customerRequestDto.getPets();
        if (idPet != null) {
            customerRepository.saveCustomerPet(idPet, customerSave.getId());
        }
        return customerSave;
    }

    @Override
    public Customer updateCustomer(String id, CustomerRequestDto customerRequestDto) {
        if (customerRepository.getCustomerById(id) == null) {
            return null;
        }
        Customer customer = customerMapper.customerRequestToCustomer(customerRequestDto);
        List<Branch> branches = new ArrayList<>();
        customerRequestDto.getBranches().forEach(branchesCode -> {
            Optional<Branch> branchSingle = branchRepository.findById(branchesCode);
            branchSingle.ifPresent(branches::add);
        });
        customer.setBranches(branches);
        customer.setId(id);
        Customer customerSave = customerRepository.save(customer);
        List<String> idPet = customerRequestDto.getPets();
        if (idPet != null) {
            customerRepository.removePetFromCustomer(id);
            customerRepository.updateCustomerPet(idPet, id);
        }
        return customerSave;
    }

    @Override
    public void deleteCustomer(String id) throws Exception {
        if (customerRepository.getCustomerById(id) == null) {
            throw new Exception("Customer is not exist");
        }
        if (customerRepository.checkExistPet(id) != null) {
            throw new Exception("Pet is exist by customer, please delete pet first");
        }
        customerRepository.deleteCustomerById(id);
    }

}
