package com.backend.Mock3project.service;

import com.backend.Mock3project.dto.SpeciesDto;
import com.backend.Mock3project.dto.SpeciesRequestDto;
import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;


@Service
public interface SpeciesService {
    /**
     * Get all Species
     * @int pageNum
     * @size pageSize
     * return List<SpeciesDto>, total number of Species
     */
    Page<SpeciesDto> getSpecies(int pageNum, int pageSize);
    /**
     * add Species
     * @SpeciesRequestDto speciesRequestDto
     */
    void saveSpecies(SpeciesRequestDto speciesRequestDto) throws NotFoundException;
    /**
     * delete Species by Id
     * @String id
     * @throws NotFoundException --throw not found
     */
    void deleteSpecies(String id) throws Exception;
    /**
     * updateSpecies Species by Id
     * @String id
     * @SpeciesRequestDto speciesRequestDto
     * @throws NotFoundException --throw not found
     */
    void updateSpecies(String id, SpeciesRequestDto speciesRequestDto) throws NotFoundException;
    /**
     * search all Species by name, 1page, size
     * @String name
     * @int pageNum
     * @int pageSize
     * @return List<SpeciesDto>
     */
    Page<SpeciesDto> searchBySpeciesName(String name, int pageNum, int pageSize);

    /**
     * Get Species By Id
     * @String id
     * @throws NotFoundException --throw not found
     * @return SpeciesDto
     */
    SpeciesDto getSpeciesById(String id) throws NotFoundException;
}
