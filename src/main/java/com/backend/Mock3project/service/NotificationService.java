package com.backend.Mock3project.service;

import com.backend.Mock3project.dto.NotificationRequest;
import com.backend.Mock3project.dto.UserDeviceRequestDto;
import com.backend.Mock3project.dto.UserNotificationDto;
import com.backend.Mock3project.entity.UserDevice;
import com.backend.Mock3project.entity.UserNotification;
import org.springframework.data.domain.Page;

import java.util.List;

public interface NotificationService {
    /**
     * Push notification
     * @PushNotificationRequest request
     * return void
     */
    void sendNotificationToToken(String userId, NotificationRequest request) throws Exception;

    /**
     * Add User Device
     * @UserDeviceRequestDto request
     * return Integer
     * @throws Exception
     */
    void addUserDevice(UserDeviceRequestDto request) throws Exception;

    /**
     * deactivate User Device
     * @PushNotificationRequest request
     * return Integer
     */
    Integer deactivateUserDevice(String token);

    /**
     * get all notification by userId
     * @String userId
     * return Page<UserNotificationDto>
     */
    Page<UserNotificationDto> getAllNotificationByUser(int page, int size, String userId);
}
