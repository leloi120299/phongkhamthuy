package com.backend.Mock3project.repository;

import com.backend.Mock3project.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public interface UserRepository extends JpaRepository<User, String> {
    /**
     * Get User by username
     * @String name
     * return User
     */
    @Query(value = "select * from \"user\" u where u.username = ?1 and u.flag <> 1", nativeQuery = true)
    User getUserByUsername(String name);

    /**
     * Get all User
     * @Pageable pageable
     * return Page<User>
     */
    @Query(value = "select * from \"user\" u where u.flag <> 1", nativeQuery = true)
    Page<User> getAllUser(Pageable pageable);

    /**
     * Search User
     * @String name
     * @String phone
     * @String role
     * @String branch
     * @Pageable pageable
     * return Page<User>
     */
    @Query(value = "select * from \"user\" u where u.flag <> 1 and lower(u.name) like lower(concat('%', ?1,'%'))" +
            " and u.phone_number like concat('%', ?2,'%') and u.id in (select user_id from user_role where role_id in (select distinct id from role where lower(role.name) like lower(concat('%', ?3,'%'))) ) and u.branch_id in (select id from branch b where lower(b.name) like lower(concat('%', ?4,'%')))"
            , countQuery = "select count(*) from \"user\" u where u.flag <> 1 and lower(u.name) like lower(concat('%', ?1,'%'))" +
            " and u.phone_number like concat('%', ?2,'%') and u.id in (select user_id from user_role where role_id in (select distinct id from role where lower(role.name) like lower(concat('%', ?3,'%'))) ) and u.branch_id in (select id from branch b where lower(b.name) like lower(concat('%', ?4,'%')))"
            , nativeQuery = true)
    Page<User> searchAllUser(String name, String phone, String role, String branch, Pageable pageable);

    /**
     * Get user by id
     * @String id
     * return User
     */
    @Query(value = "select * from \"user\" u where u.id = ?1 and u.flag <> 1", nativeQuery = true)
    User getUserById(String id);

    /**
     * Get User doctor by id
     * @String id
     * return User
     */
    @Query(value = "select * from \"user\" u where u.id = ?1 and u.flag <> 1 and (select role_id from user_role r where r.user_id = ?1) = 1", nativeQuery = true)
    User getDocById(String id);

    /**
     * Delete User by Id
     * @String id
     * void Integer
     */
    @Query(value = "update \"user\" set flag = 1 where id = ?1", nativeQuery = true)
    @Modifying
    void deleteUserById(String id);

    /**
     * Delete User device by user id
     * @String id
     * return void
     */
    @Query(value = "update user_device set flag = 1 where user_id = ?1", nativeQuery = true)
    @Modifying
    void deleteUserDeviceByUser(String id);

    /**
     * Delete User notification by user id
     * @String id
     * return void
     */
    @Query(value = "update user_notification set flag = 1 where user_id = ?1", nativeQuery = true)
    @Modifying
    void deleteUserNotificationByUser(String id);

    /**
     * change password by username
     * @String username
     * @String newPassword
     * return void
     */
    @Query(value = "update \"user\" set password = ?2 where username = ?1", nativeQuery = true)
    @Modifying
    void changePasswordByUsername(String username, String newPassword);

    /**
     * forgot password by username
     * @String username
     * @String newPassword
     * @String gmail
     * return void
     */
    @Query(value = "update \"user\" set password = ?2 where username = ?1 and email = ?3", nativeQuery = true)
    @Modifying
    void forgotPasswordByUsername(String username, String newPassword, String email);

    /**
     * check exist user in schedule
     * @String userId
     * return String
     */
    @Query(value = "select distinct s.user_id from schedule s where (s.user_id = ?1 or s.real_doc_id = ?1 or s.plan_doc_id = ?1) and s.flag <> 1", nativeQuery = true)
    String checkExistSchedule(String userId);
}
