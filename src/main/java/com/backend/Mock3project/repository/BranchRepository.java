package com.backend.Mock3project.repository;

import com.backend.Mock3project.entity.Branch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BranchRepository extends JpaRepository<Branch, String>, PagingAndSortingRepository<Branch, String> {

    /**
     * get all branch by page and flag
     *
     * @return List<Branch>
     * @Integer flag
     * @Pageable pageable
     */
    Page<Branch> findAllByFlagEquals(Integer flag, Pageable pageable);

    /**
     * get all branch by name,address,phone and page
     *
     * @return List<Branch>
     * @String name
     * @String address
     * @String phone
     * @Pageable pageable
     */
    @Query(value = "select b from Branch b where b.flag <> 1 and lower(b.name) like lower(concat('%', ?1,'%'))" +
            " and lower(b.address) like lower(concat('%', ?2,'%'))" + "and b.phoneNumber like concat('%', ?3,'%')")
    Page<Branch> searchAllBranch(String name, String address, String phone, Pageable pageable);

    /**
     * get branch by id
     *
     * @return Optional<Branch>
     * @String id
     */
    @Query(value = "select b from Branch b where b.id = ?1 and b.flag <> 1")
    Optional<Branch> findById(String id);

    /**
     * delete branch related to other table
     * @return String
     * @String id
     */
    @Query(value = "update branch set flag = 1 WHERE (SELECT cb.branch_id FROM customer_branch cb inner join customer c\n" +
            "on c.id = cb.customer_id where branch_id = ?1 and c.flag <> 1 LIMIT 1) is null and\n" +
            "(SELECT u.id FROM \"user\" u inner join branch b on b.id = u.branch_id WHERE\n" +
            "u.flag <> 1 and b.id = ?1 LIMIT 1) is null and id = ?1 RETURNING branch.id", nativeQuery = true)
    String deleteBranch(String id);
}
