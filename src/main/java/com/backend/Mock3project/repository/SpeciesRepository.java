package com.backend.Mock3project.repository;

import com.backend.Mock3project.entity.Species;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SpeciesRepository extends JpaRepository<Species, String>, PagingAndSortingRepository<Species, String> {
    /**
     * get all Species by page and flag
     * @Integer flag
     * @Pageable pageable
     * @return List<Species>
     */
    Page<Species> findAllByFlagEquals(Integer flag, Pageable pageable);

    /**
     * get all Species by name,address,phone and page
     * @String name
     * @Pageable pageable
     * @return List<Species>
     */
    @Query(value = "select s from Species s where s.flag <> 1 and lower(s.name) like lower(concat('%', ?1,'%'))")
    Page<Species> findAllByNameIsContainingIgnoreCase(String name, Pageable pageable);

    /**
     * get Species by id
     * @String id
     * @return Optional<Species>
     */
    @Query(value = "select s from Species s where s.id = ?1 and s.flag <> 1")
    Optional<Species> findById(String id);

    /**
     * delete Species by id
     * @String id
     * @return String
     */
    @Query(value = "update species set flag = 1 WHERE (SELECT ps.species_id FROM pet_species ps inner join " +
            "pet p on p.id = ps.pet_id where p.flag <> 1 and species_id = ?1 LIMIT 1) is null and id = ?1 " +
            "RETURNING species.id", nativeQuery = true)
    String deleteSpecies(String id);


    /**
     * countAllSearchSpecies
     * @String name
     * @return long
     */
    @Query(value = "select count(s) from Species s where s.flag <> 1 and lower(s.name) like lower(concat('%', ?1,'%'))")
    Long countAllSearchSpecies(String name);

    /**
     * get Species by type_id
     * @String type_id
     * @return List<Species>
     */
    @Query(value = "select * from Species s where s.flag <> 1 and s.type_id = ?1", nativeQuery = true)
    List<Species> searchByTypeId(String id);
}
