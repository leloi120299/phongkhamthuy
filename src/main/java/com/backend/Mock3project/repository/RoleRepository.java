package com.backend.Mock3project.repository;

import com.backend.Mock3project.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    /**
     * find role by name
     * @String name
     * return Role
     */
    Role findByName(String name);
}
