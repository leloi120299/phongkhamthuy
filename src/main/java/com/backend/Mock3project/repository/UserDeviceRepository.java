package com.backend.Mock3project.repository;

import com.backend.Mock3project.entity.UserDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserDeviceRepository extends JpaRepository<UserDevice, Integer> {
    /**
     * Get User by token
     * @String name
     * return UserDevice
     */
    @Query(value = "select * from user_device u where u.token = ?1 and u.flag <> 1", nativeQuery = true)
    UserDevice getUserByToken(String token);

    /**
     * active user token by login
     * @String id
     * return Integer
     */
    @Query(value = "update user_device set status = 'login', user_id = ?1 where token = ?2 and flag <> 1", nativeQuery = true)
    @Modifying
    Integer loginUser(String userId, String token);

    /**
     * deactivate user token by logout
     * @String id
     * return Integer
     */
    @Query(value = "update user_device set status = 'logout' where token = ?1 and flag <> 1", nativeQuery = true)
    @Modifying
    Integer logoutUser(String id);

    /**
     * get token by user
     * @String userId
     * return List<String>
     */
    @Query(value = "select token from user_device u where u.user_id = ?1 and u.flag <> 1 and u.status = 'login'", nativeQuery = true)
    List<String> getTokenByUser(String userId);
}
