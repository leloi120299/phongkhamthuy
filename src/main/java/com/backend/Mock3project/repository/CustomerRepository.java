package com.backend.Mock3project.repository;

import com.backend.Mock3project.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {
    /**
     * save pet for Customer
     *
     * @List<String> pets
     * @String cus_id
     * return void
     */
    @Query(value = "update Pet set customer_id = :cus_id where id in :pets and customer_id IS NULL and flag <> 1", nativeQuery = true)
    @Modifying
    void saveCustomerPet(@Param("pets") List<String> pets, @Param("cus_id") String cus_id);

    /**
     * update pet for Customer
     *
     * @List<String> pets
     * @String cus_id
     * return void
     */
    @Query(value = "update Pet set customer_id = :cus_id where id in :pets and flag <> 1 and customer_id IS NULL", nativeQuery = true)
    @Modifying
    void updateCustomerPet(@Param("pets") List<String> pets, @Param("cus_id") String cus_id);

    /**
     * remove pet for Customer
     *
     * @String cus_id
     * return void
     */
    @Query(value = "update Pet set customer_id = null where customer_id = :cus_id and flag <> 1", nativeQuery = true)
    @Modifying
    void removePetFromCustomer(@Param("cus_id") String cus_id);

    /**
     * get all Customer
     *
     * @Pageable pageable
     * return Page<Customer>
     */
    @Query(value = "select * from customer c where c.flag <> 1", nativeQuery = true)
    Page<Customer> getAllCustomer(Pageable pageable);

    /**
     * search all Customer
     *
     * @String name
     * @String branch
     * @Pageable pageable
     * return Page<Customer>
     */
    @Query(value = "select * from customer c where c.flag <> 1 and lower(c.name) like lower(concat('%', ?1,'%')) and c.id in (select customer_id from customer_branch where lower(branch_id) like lower(concat('%', ?2,'%')))", nativeQuery = true)
    Page<Customer> searchAllCustomer(String name, String branch, Pageable pageable);

    /**
     * get customer by id
     *
     * @String id
     * return Customer
     */
    @Query(value = "select * from customer c where c.id = ?1 and c.flag <> 1", nativeQuery = true)
    Customer getCustomerById(String id);

    /**
     * delete customer by id
     *
     * @String id
     * return int
     */
    @Query(value = "update customer set flag = 1 where id = ?1 and flag <> 1", nativeQuery = true)
    @Modifying
    int deleteCustomerById(String id);

    /**
     * check pet is exist by customer
     * @String customerId
     * return String
     */
    @Query(value = "select distinct p.customer_id from pet p where p.customer_id = ?1 and p.flag <> 1", nativeQuery = true)
    String checkExistPet(String customerId);
}
