package com.backend.Mock3project.repository;

import com.backend.Mock3project.entity.Pet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


public interface PetRepository extends JpaRepository<Pet, String> {
    /**
     * get all pet
     * @Pageable pageable
     * return Page<Pet>
     */
    @Query(value = "select * from pet p where p.flag <> 1", nativeQuery = true)
    Page<Pet> getAllPet(Pageable pageable);

    /**
     * search all pet
     * @String name
     * @Pageable pageable
     * return Page<Pet>
     */
    @Query(value = "select * from Pet p where p.flag <> 1 and lower(p.name) like lower(concat('%', ?1,'%'))", nativeQuery = true)
    Page<Pet> searchAllPet(String name, Pageable pageable);

    /**
     * get pet by id
     * @String id
     * return Pet
     */
    @Query(value = "select * from pet p where p.id = ?1 and p.flag <> 1", nativeQuery = true)
    Pet getPetById(String id);

    /**
     * delete pet by id
     * @String id
     * return Integer
     */
    @Query(value = "update pet set flag = 1, customer_id = null where id = ?1", nativeQuery = true)
    @Modifying
    Integer deletePetById(String id);

    /**
     * check exist pet in schedule
     * @String petId
     * return String
     */
    @Query(value = "select distinct s.pet_id from schedule s where s.pet_id = ?1 and s.flag <> 1", nativeQuery = true)
    String checkExistSchedule(String petId);
}
