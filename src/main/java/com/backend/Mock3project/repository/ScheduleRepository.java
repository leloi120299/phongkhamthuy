package com.backend.Mock3project.repository;

import com.backend.Mock3project.entity.Schedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {

    /**
     * Get all schedule
     * @Pageable pageable
     * @return List<Schedule>
     */
    @Query(value = "select * from public.schedule s where s.flag <> 1", nativeQuery = true)
    Page<Schedule> getAllSchedule(Pageable pageable);

    /**
     * Count schedule
     * @return Integer
     */
    @Query(value = "select count(s) from public.schedule s where s.flag <> 1", nativeQuery = true)
    Integer countSchedules();

    /**
     * Search schedule by name of customer
     * @String name
     * @Pageable pageable
     * @return List<Schedule>
     */
    @Query(value = "select * from schedule s inner join pet p on s.pet_id = p.id inner join customer c on p.customer_id = c.id " +
            "where s.flag <> 1 and lower(c.name) like lower(concat('%', ?1,'%'))", nativeQuery = true)
    Page<Schedule> searchScheduleByNameCustomer(String name, Pageable pageable);

    /**
     * Count schedule by name customer
     * @String name
     * @return Integer
     */
    @Query(value = "select count(s) from schedule s inner join pet p on s.pet_id = p.id inner join customer c on p.customer_id = c.id " +
            "where s.flag <> 1 and lower(c.name) like lower(concat('%', ?1,'%'))", nativeQuery = true)
    Integer countScheduleByNameCustomers(String name);

    /**
     * Delete schedule by id
     * @Integer id
     * @return Integer
     */
    @Query(value = "update Schedule set flag = 1 where id = ?1", nativeQuery = true)
    @Modifying
    Integer deleteScheduleById(Integer id);

    /**
     * Search schedule by status
     * @String status
     * @return List<Schedule>
     */
    @Query(value = "select * from schedule s where s.flag <> 1 and s.status = ?1", nativeQuery = true)
    List<Schedule> findByStatus(String status);

    /**
     * Count schedule by status
     * @String status
     * @return Integer
     */
    @Query(value = "select count(s.id) from schedule s where s.flag <> 1 and s.status = ?1", nativeQuery = true)
    Integer countAllByStatus(String status);

    /**
     * Search schedule by id
     * @Integer id
     * @return Schedule
     */
    @Query(value = "select * from schedule s where s.flag <> 1 and s.id = ?1",nativeQuery = true)
    Optional<Schedule> findById(Integer id);

    /**
     * Search schedule by branch id
     * @String id
     * @Pageable pageable
     * @return List<Schedule>
     */
    @Query(value = "select s.* from schedule s inner join \"user\" u on u.id = s.user_id " +
            "inner join branch b on u.branch_id = b.id where s.flag <> 1 and b.id = ?1", nativeQuery = true)
    Page<Schedule> findSchedulesByBranchId(String id, Pageable pageable);

    /**
     * Count schedule by branch id
     * @String id
     * @return Integer
     */
    @Query(value = "select count(s) from schedule s inner join \"user\" u on u.id = s.user_id " +
            "inner join branch b on u.branch_id = b.id where s.flag <> 1 and b.id = ?1", nativeQuery = true)
    Integer countSchedulesByBranchId(String id);

    /**
     * Search schedule by doc id
     * @String id
     * @Pageable pageable
     * @return List<Schedule>
     */
    @Query(value = "select s.* from schedule s inner join \"user\" u on u.id = s.user_id " +
            "where s.flag <> 1 and s.plan_doc_id =?1 and (select r.role_id from user_role r where r.user_id = ?1) = 1", nativeQuery = true)
    Page<Schedule> findSchedulesByDocId(String id, Pageable pageable);

    /**
     * Count schedule by doc id
     * @String id
     * @return Integer
     */
    @Query(value = "select count(s) from schedule s inner join \"user\" u on u.id = s.user_id " +
            "where s.flag <> 1 and s.plan_doc_id =?1 and (select r.role_id from user_role r where r.user_id = ?1) = 1", nativeQuery = true)
    Integer countSchedulesByDocId(String id);

    /**
     * Search schedule by date and time
     * @LocalDate date
     * @LocalTime time
     * @Pageable pageable
     * @return List<Schedule>
     */
    @Query(value = "select s.* from schedule s where s.date = ?1 and s.time =?2 and flag <> 1",nativeQuery = true)
    Page<Schedule> findByDateAndTime(LocalDate date, LocalTime time, Pageable pageable);

    /**
     * Count by date and time
     * @LocalDate date
     * @LocalTime time
     * @return Integer
     */
    @Query(value = "select count(s) from schedule s where s.date = ?1 and s.time =?2 and flag <> 1",nativeQuery = true)
    Integer countByDateAndTime(LocalDate date, LocalTime time);

    /**
     * Search schedule by interval date
     * @Date date1
     * @Date date2
     * @Pageable pageable
     * @return List<Schedule>
     */
    @Query(value = "select s.* from schedule s where  flag <> 1 and s.date >= ?1 and s.date <= ?2", nativeQuery = true)
    Page<Schedule> findSchedulesByDateInterval(Date date1, Date date2, Pageable pageable);

    /**
     * Count schedule by interval date
     * @Date date1
     * @Date date2
     * @return Integer
     */
    @Query(value = "select count(s) from schedule s where  flag <> 1 and s.date >= ?1 and s.date <= ?2", nativeQuery = true)
    Integer countSchedulesByDateInterval(Date date1, Date date2);

    /**
     * Get schedule detail
     * @List<Integer> scheduleId
     * @return List<Object[]>
     */
    @Query(value = "select pdoc.id as planDocId, pdoc.name as planDocName, pdoc.phone_number as planDocPhone, " +
            "pdoc.specialization as planDocSpec, rdoc.id as realDocId, rdoc.name as realDocName, rdoc.phone_number as realDocPhone, " +
            "rdoc.specialization as realDocSpec, c.id as customerId, c.name as customerName, c.phone_number as customerPhone " +
            "from  (schedule s left join \"user\" pdoc on s.plan_doc_id = pdoc.id left join \"user\" rdoc on s.real_doc_id = rdoc.id " +
            "left join customer c on c.id = (select customer_id from pet " +
            "where pet.id = s.pet_id)) where s.flag <> 1 and s.id in ?1", nativeQuery = true)
    List<Object[]> getScheduleDetail(List<Integer> scheduleId);

    /**
     * Get schedule detail by id
     * @Integer id
     * @return List<Object[]>
     */
    @Query(value = "select pdoc.id as planDocId, pdoc.name as planDocName, pdoc.phone_number as planDocPhone, " +
            "pdoc.specialization as planDocSpec, rdoc.id as realDocId, rdoc.name as realDocName, rdoc.phone_number as realDocPhone, " +
            "rdoc.specialization as realDocSpec, c.id as customerId, c.name as customerName, c.phone_number as customerPhone " +
            "from  (schedule s left join \"user\" pdoc on s.plan_doc_id = pdoc.id left join \"user\" rdoc on s.real_doc_id = rdoc.id " +
            "left join customer c on c.id = (select customer_id from pet " +
            "where pet.id = s.pet_id)) where s.flag <> 1 and s.id = ?1", nativeQuery = true)
    List<Object[]> getScheduleDetailById(Integer id);

}
