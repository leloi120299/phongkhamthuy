package com.backend.Mock3project.repository;

import com.backend.Mock3project.entity.UserNotification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface UserNotificationRepository extends JpaRepository<UserNotification, Integer> {
    /**
     * check exist message
     * @Pageable pageable
     * return Page<UserNotification>
     */
    @Query(value = "select * from user_notification where user_id = ?1 and flag <> 1", nativeQuery = true)
    Page<UserNotification> getAllUserNotification(String userId, Pageable pageable);
}
