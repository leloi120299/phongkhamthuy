package com.backend.Mock3project.repository;

import com.backend.Mock3project.entity.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface TypeRepository extends JpaRepository<Type, String>, PagingAndSortingRepository<Type,String> {
    /**
     * get all Type by page and flag
     * @Pageable pageable
     * @return List<Type>
     */
    @Query(value = "select * from Type t where t.flag <> 1", nativeQuery = true)
    Page<Type> getListType(Pageable pageable);

    /**
     * Get all Type by name
     * @String name
     * @Pageable pageable
     * @return List<Type>
     */
    @Query(value = "select * from Type t where t.flag <> 1 and lower(t.name) like lower(concat('%', ?1 , '%'))", nativeQuery = true)
    Page<Type> searchByName(String name, Pageable pageable);

    /**
     * Get type by Id
     * @String id
     * @return Optional<Type>
     */
    @Query(value = "select t from Type t where t.id = ?1 and t.flag <> 1")
    Optional<Type> getTypeById(String id);

    /**
     * Get name of type by Id
     * @param id
     * @return String
     */
    @Query(value = "select t.name from Type t where t.flag <> 1 and t.id =?1")
    String getNameById(String id);
}
