package com.backend.Mock3project.mapper;

import com.backend.Mock3project.dto.PetDto;
import com.backend.Mock3project.dto.PetRequestDto;
import com.backend.Mock3project.entity.Pet;
import com.backend.Mock3project.enumeration.Gender;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import static com.backend.Mock3project.utils.Constants.LINK_IMG;


@Mapper(componentModel = "spring")
public interface PetMapper {
    /**
     * gender url img
     * @String birthCerti
     * return String
     */
    @Named("genderUrlImg")
    static String genderUrlImg(String birthCerti) {
        return LINK_IMG + birthCerti;
    }

    /**
     * gender render
     * @int sex
     * return String
     */
    @Named("genderRender")
    static String genderRender(int sex) {
        return Gender.getTextByCode(sex);
    }

    /**
     * transfer PetRequestDto to Pet
     * @PetRequestDto petRequestDto
     * return Pet
     */
    @Mapping(source = "sex", target = "sex", qualifiedByName = "genderRender")
    @Mapping(target = "species", ignore = true)
    @Mapping(target = "birthCerti", ignore = true)
    Pet petRequestToPet(PetRequestDto petRequestDto);

    /**
     * transfer Pet to PetDto
     * @Pet pet
     * return PetDto
     */
    @Mapping(source = "birthCerti", target = "birthCerti", qualifiedByName = "genderUrlImg")
    PetDto petToPetDto(Pet pet);
}
