package com.backend.Mock3project.mapper;

import com.backend.Mock3project.dto.UserDto;
import com.backend.Mock3project.dto.UserRequestDto;
import com.backend.Mock3project.entity.Branch;
import com.backend.Mock3project.entity.User;
import com.backend.Mock3project.enumeration.Gender;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface UserMapper {
    /**
     * init branch by branch id
     * @String branchId
     * return Branch branch
     */
    @Named("branchInit")
    static Branch branchRender(String branchId) {
        Branch branch = new Branch();
        branch.setId(branchId);
        return branch;
    }

    /**
     * gender render
     * @int sex
     * return String
     */
    @Named("genderRender")
    static String genderRender(int sex) {
        return Gender.getTextByCode(sex);
    }

    /**
     * transfer UserRequestDto to User
     * @UserRequestDto userRequestDto
     * return User
     */
    @Mapping(target = "roles", ignore = true)
    @Mapping(source = "branchId", target = "branch", qualifiedByName = "branchInit")
    @Mapping(source = "sex", target = "sex", qualifiedByName = "genderRender")
    User userRequestToUser(UserRequestDto userRequestDto);

    /**
     * transfer User to UserDto
     * @User user
     * return UserDto
     */
    UserDto userToUserDto(User user);
}
