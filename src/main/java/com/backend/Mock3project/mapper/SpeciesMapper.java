package com.backend.Mock3project.mapper;

import com.backend.Mock3project.dto.SpeciesDto;
import com.backend.Mock3project.dto.SpeciesRequestDto;
import com.backend.Mock3project.entity.Species;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;


import java.util.List;

@Mapper(componentModel = "spring")
public interface SpeciesMapper {

    SpeciesDto speciesToSpeciesDTO(Species species);

    Species speciesRequestDTOToSpecies(SpeciesRequestDto speciesRequestDto);

    List<SpeciesDto> listSpeciesToListSpeciesDTO(List<Species> species);
    default Page<SpeciesDto> pageSpeciesToListSpeciesDTO(Page<Species> species){
        return species.map(this::speciesToSpeciesDTO);
    };
}
