package com.backend.Mock3project.mapper;

import com.backend.Mock3project.dto.ScheduleDto;
import com.backend.Mock3project.dto.ScheduleRequestDto;
import com.backend.Mock3project.entity.Pet;
import com.backend.Mock3project.entity.Schedule;
import com.backend.Mock3project.entity.User;
import com.backend.Mock3project.enumeration.ScheduleType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;


import java.util.List;

@Mapper(componentModel = "spring")
public interface ScheduleMapper {
    @Named("user")
    default User genderUser(String userId){
        User user = new User();
        user.setId(userId);
        return user;
    }

    @Named("pet")
    default Pet genderPet(String petId){
        Pet pet = new Pet();
        pet.setId(petId);
        return pet;
    }

    @Named("setStatus")
    static String statusRender(int status) {
        return ScheduleType.getTextByCode(status);
    }

    /*
    * Map schedule-request-dto to schedule-entity
    * @param scheduleRequestDto
    * */
    @Mapping(source = "status", target = "status", qualifiedByName = "setStatus")
    @Mapping(source = "userId", target = "user", qualifiedByName = "user")
    @Mapping(source = "petId", target = "pet", qualifiedByName = "pet")
    public Schedule mapDtoToEntity(ScheduleRequestDto scheduleRequestDto);

    /*
    * Map schedule-entity to schedule-dto
    * @param schedule
    * */
    @Mapping(target = "additionalDetail", ignore = true)
    public ScheduleDto mapEntityToDto(Schedule schedule);

    /*
    * Map schedule-entity to list-schedule-dto
    * @param schedules
    * */
    @Mapping(target = "planDoc", ignore = true)
    @Mapping(target = "realDoc", ignore = true)
    public List<ScheduleDto> mapEntityToDtos(List<Schedule> schedules);
}
