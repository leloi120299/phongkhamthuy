package com.backend.Mock3project.mapper;

import com.backend.Mock3project.dto.BranchDto;
import com.backend.Mock3project.dto.BranchRequestDto;
import com.backend.Mock3project.entity.Branch;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BranchMapper {

    //    @Mapping(target = "id", source = "branch.id")
//    @Mapping(target = "name", source = "branch.name")
//    @Mapping(target = "address", source = "branch.address")
//    @Mapping(target = "phoneNumber", source = "branch.phoneNumber")
//    @Mapping(target = "note", source = "branch.note")
    BranchDto branchToBranchDTO(Branch branch);

    Branch branchRequestDTOToBranch(BranchRequestDto branchRequestDto);

    List<BranchDto> listBranchToListBranchDTO(List<Branch> branches);
    default Page<BranchDto> pageBranchToListBranchDTO(Page<Branch> branches){
        return branches.map(this::branchToBranchDTO);
    };

}
