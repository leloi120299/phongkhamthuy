package com.backend.Mock3project.mapper;

import com.backend.Mock3project.dto.NotificationRequest;
import com.backend.Mock3project.dto.UserDeviceRequestDto;
import com.backend.Mock3project.dto.UserNotificationDto;
import com.backend.Mock3project.entity.User;
import com.backend.Mock3project.entity.UserDevice;
import com.backend.Mock3project.entity.UserNotification;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Mapper(componentModel = "spring")
public interface UserDeviceMapper {
    /**
     * render date
     * @Date date
     * return String
     */
    @Named("renderDate")
    static String renderDate(Date date) {
        if (date == null) {
            return null;
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return dateFormat.format(date);
    }

    /**
     * init user by user id
     * @String userId
     * return User user
     */
    @Named("userInit")
    static User userRender(String userId) {
        User user = new User();
        user.setId(userId);
        return user;
    }

    /**
     * transfer UserDeviceRequestDto to UserDevice
     * @UserDeviceRequestDto userDeviceRequestDto
     * return UserDevice
     */
    @Mapping(source = "userId", target = "user", qualifiedByName = "userInit")
    UserDevice userDeviceRequestToUserDevice(UserDeviceRequestDto userDeviceRequestDto);


    /**
     * transfer UserDeviceRequestDto to UserNotification
     * @UserDeviceRequestDto userDeviceRequestDto
     * return UserNotification
     */
    UserNotification userNotificationRequestToUserNotification(NotificationRequest notificationRequest);

    /**
     * transfer UserNotification to UserNotificationDto
     * @UserNotification userNotification
     * return UserNotificationDto
     */
    @Mapping(source = "date", target = "date", qualifiedByName = "renderDate")
    UserNotificationDto userNotificationToUserNotificationDto(UserNotification userNotification);
}
