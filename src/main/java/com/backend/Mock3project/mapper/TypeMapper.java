package com.backend.Mock3project.mapper;

import com.backend.Mock3project.dto.TypeDto;
import com.backend.Mock3project.dto.TypeRequestDto;
import com.backend.Mock3project.entity.Type;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TypeMapper {


    /**
    * Map type to typeDto
    * @Param type
    * return typeDto
    */

    TypeDto typeToTypeDTO(Type type);

    /**
     * Map typeRequestDto to Type
     * @Param typeRequestDto
     * return Type
     */
    Type typeRequestDTOToType(TypeRequestDto typeRequestDto);

    /**
     * Map list type to list typeDto
     * @Param List<Type>
     * return List<TypeDto>
     */
    List<TypeDto> listTypeToListTypeDTO(List<Type> types);
}
