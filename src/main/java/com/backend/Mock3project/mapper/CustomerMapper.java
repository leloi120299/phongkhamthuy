package com.backend.Mock3project.mapper;

import com.backend.Mock3project.dto.CustomerDto;
import com.backend.Mock3project.dto.CustomerRequestDto;
import com.backend.Mock3project.dto.PetDto;
import com.backend.Mock3project.entity.Customer;
import com.backend.Mock3project.entity.Pet;
import com.backend.Mock3project.enumeration.CustomerType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public interface CustomerMapper {
    static final PetMapper MAPPER = Mappers.getMapper(PetMapper.class);
    /**
     * gender gender Pet to PetDto
     * @List<Pet> pets
     * return List<PetDto>
     */
    @Named("genderPetToPetDto")
    static List<PetDto> genderUrlImg(List<Pet> pets) {
        return pets.stream().
                map(MAPPER::petToPetDto).collect(Collectors.toList());
    }


    /**
     * type render
     * @int type
     * return String
     */
    @Named("setType")
    static String setType(int type) {
        return CustomerType.getTextByCode(type);
    }

    /**
     * transfer CustomerRequestDto to Customer
     * @CustomerRequestDto customerRequestDto
     * return Customer
     */
    @Mapping(target = "pets", ignore = true)
    @Mapping(target = "branches", ignore = true)
    @Mapping(source = "type", target = "type", qualifiedByName = "setType")
    Customer customerRequestToCustomer(CustomerRequestDto customerRequestDto);

    @Mapping(source = "pets", target = "pets", qualifiedByName = "genderPetToPetDto")
    CustomerDto customerToCustomerDto(Customer customer);
}
