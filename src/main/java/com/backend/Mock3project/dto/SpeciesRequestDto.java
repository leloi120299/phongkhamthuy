package com.backend.Mock3project.dto;

import com.backend.Mock3project.entity.Type;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.Nationalized;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class SpeciesRequestDto {

    private String id;

    @NotBlank(message = "Species name can not be blank")
    @Nationalized
    private String name;

    @Size(max = 500)
    private String note;

    @JsonIgnore
    @JsonProperty(value = "flag")
    private Integer flag = 0;

    @NotNull
    private String typeId;

    private Type type;
}
