package com.backend.Mock3project.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.Pattern;

import java.util.List;

import static com.backend.Mock3project.utils.Constants.REGEX_CMTND;
import static com.backend.Mock3project.utils.Constants.REGEX_PHONENUMBER;

@Data
public class CustomerRequestDto {
    @NotNull(message = "Customer: name must not be null")
    private String name;

    @NotNull(message = "Customer: address must not be null")
    private String address;

    @NotNull(message = "Customer: phoneNumber must not be null")
    @Pattern(regexp = REGEX_PHONENUMBER, message = "Phone must be number and 10 or 11 digits")
    private String phoneNumber;

    @NotNull(message = "Customer: email must not be null")
    @Email(message = "Email not right")
    private String email;

    @NotNull(message = "Customer: type must not be null")
    private int type;

    private List<String> pets;

    @JsonIgnore
    private Integer flag = 0;

//    @NotNull(message = "Customer: branchId must not be null")
    private List<String> branches;
}
