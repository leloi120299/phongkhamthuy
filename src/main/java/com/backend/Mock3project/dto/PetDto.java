package com.backend.Mock3project.dto;

import com.backend.Mock3project.entity.Species;
import lombok.Data;

import java.util.List;

@Data
public class PetDto {
    private String id;
    private String name;
    private String sex;
    private String birthCerti;
    private String note;
    private String customerId;
    private List<Species> species;
}
