package com.backend.Mock3project.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@Data
public class PetRequestDto {
    @NotNull(message = "Pet: name must not be null")
    private String name;

    @NotNull(message = "Pet: sex must not be null")
    private int sex;

    @NotNull(message = "Pet: birthCerti must not be null")
    private MultipartFile birthCerti;

    private String note;

    private String customerId;

    @NotNull(message = "Species must not be null")
    private String species;

    @JsonIgnore
    private Integer flag = 0;

}
