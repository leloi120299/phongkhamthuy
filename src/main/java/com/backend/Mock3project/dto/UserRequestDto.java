package com.backend.Mock3project.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

import static com.backend.Mock3project.utils.Constants.REGEX_CMTND;
import static com.backend.Mock3project.utils.Constants.REGEX_PHONENUMBER;

@Data
public class UserRequestDto {
    @NotNull(message = "User Name must not be null")
    private String name;

    @NotNull(message = "User sex must not be null")
    private Integer sex;

    @NotNull(message = "User birth must not be null")
    private Date birth;

    @NotNull(message = "User identityCard must not be null")
    @Pattern(regexp = REGEX_CMTND, message = "id card must have 9 to 12 numbers")
    private String identityCard;

    @NotNull(message = "User phoneNumber must not be null")
    @Pattern(regexp = REGEX_PHONENUMBER, message = "Phone must be number and 10 or 11 digits")
    private String phoneNumber;

    @NotNull(message = "User address must not be null")
    private String address;

    private String specialization;

    private String experience;

    @NotNull(message = "User username must not be null")
    private String username;

    @NotNull(message = "User password must not be null")
    private String password;

    @NotNull(message = "User email must not be null")
    @Email(message = "Email not right")
    private String email;

    @NotNull(message = "User branchId must not be null")
    private String branchId;

    @NotNull(message = "User role must not be null")
    private List<Integer> roles;

    @JsonIgnore
    private Integer flag = 0;

    private String note;
}
