package com.backend.Mock3project.dto;

import lombok.Data;
import org.hibernate.annotations.Nationalized;

import javax.validation.constraints.NotBlank;

@Data
public class TypeRequestDto {

    private String id;

    @NotBlank(message = "Type Name must not be blank")
    @Nationalized
    private String name;

    @Nationalized
    private String note;

    private Integer flag = 0;
}
