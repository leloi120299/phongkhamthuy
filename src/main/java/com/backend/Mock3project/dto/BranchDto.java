package com.backend.Mock3project.dto;

import com.backend.Mock3project.entity.User;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonFilter("branch-filtering-dashboard")
public class BranchDto {

    private String id;
    private String name;
    private String address;
    private String phoneNumber;
    private String note;
    @JsonIgnore
    private Integer flag;
    @JsonIgnore
    private List<User> users;
    private int customerNumber;
    private int petNumber;
    private int typeNumber;
    private int speciesNumber;
    private int userNumber;
}
