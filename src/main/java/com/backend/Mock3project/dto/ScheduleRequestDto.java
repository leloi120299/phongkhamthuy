package com.backend.Mock3project.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class ScheduleRequestDto {
    private Integer id;

    @NotNull(message = "Schedule date must not be null")
    @JsonFormat(pattern = "dd-MM-yyyy", shape = JsonFormat.Shape.STRING)
    private LocalDate date;

    @NotNull(message = "Schedule time must not be null")
    @JsonFormat(pattern = "HH:mm", shape = JsonFormat.Shape.STRING)
    private LocalTime time;

    @NotBlank(message = "Schedule symptom must not be blank")
    private String symptom;

    private String note;

    @Min(value = 1, message = "Status can not be less than 1")
    @Max(value = 3, message = "Status can not be greater than 3")
    private Integer status = 2;

    private String planDocId;

    private String realDocId;

    private Integer flag = 0;

    @NotBlank(message = "Pet ID must not be blank")
    private String petId;

    @NotBlank(message = "User ID must not be blank")
    private String userId;
}
