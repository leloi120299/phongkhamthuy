package com.backend.Mock3project.dto;

import com.backend.Mock3project.entity.Branch;
import com.backend.Mock3project.entity.Pet;
import lombok.Data;

import java.util.List;

@Data
public class CustomerDto {
    private String id;
    private String name;
    private String address;
    private String phoneNumber;
    private String email;
    private String type;
    private List<PetDto> pets;
    private List<Branch> branches;
}
