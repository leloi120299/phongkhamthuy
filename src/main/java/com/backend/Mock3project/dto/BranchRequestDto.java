package com.backend.Mock3project.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.Nationalized;
import org.springframework.web.bind.annotation.ControllerAdvice;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.backend.Mock3project.utils.Constants.REGEX_PHONENUMBER;

@Data
@ControllerAdvice
public class BranchRequestDto {

    private String id;

    @NotBlank(message = "Branch Name must not be blank")
    @Nationalized
    private String name;

    @NotBlank(message = "Branch Address must not be blank")
    @Nationalized
    @Size(max = 500)
    private String address;

    @Pattern(regexp=REGEX_PHONENUMBER,message = "Phone must be number and 10 or 11 digits")
    @NotBlank(message = "Phone Number must not be blank")
    private String phoneNumber;

    @JsonIgnore
    @JsonProperty(value = "flag")
    private Integer flag = 0;

    @Nationalized
    @Size(max = 500)
    private String note;
}
