package com.backend.Mock3project.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserDeviceRequestDto {
    @NotNull(message = "User token must not be null")
    private String token;

    @NotNull(message = "User id must not be null")
    private String userId;
}
