package com.backend.Mock3project.dto;

import lombok.Data;

@Data
public class UserNotificationDto {
    private String title;

    private String message;

    private String topic;

    private String date;
}
