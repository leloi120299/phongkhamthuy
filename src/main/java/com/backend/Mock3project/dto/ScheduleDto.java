package com.backend.Mock3project.dto;

import com.backend.Mock3project.entity.Pet;
import com.backend.Mock3project.entity.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class ScheduleDto {
    private Integer id;

    @JsonFormat(pattern = "dd-MM-yyyy", shape = JsonFormat.Shape.STRING)
    private LocalDate date;
    @JsonFormat(pattern = "HH:mm", shape = JsonFormat.Shape.STRING)
    private LocalTime time;
    private String symptom;
    private String note;
    private String status;
    private User user;
    private Pet pet;
    private ScheduleAdditionalDataDto additionalDetail;
}
