package com.backend.Mock3project.dto;

import com.backend.Mock3project.entity.Type;
import lombok.Data;

@Data
public class SpeciesDto {
    private String id;
    private String name;
    private String note;
    private Type type;
}
