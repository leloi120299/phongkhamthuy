package com.backend.Mock3project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleAdditionalDataDto {
    String planDocId;
    String planDocName;
    String planDocPhone;
    String planDocSpec;
    String realDocId;
    String realDocName;
    String realDocPhone;
    String realDocSpec;
    String customerId;
    String customerName;
    String customerPhone;
}
