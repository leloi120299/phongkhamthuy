package com.backend.Mock3project.dto;

import lombok.Data;
import org.hibernate.annotations.Nationalized;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RoleRequestDto {
    @NotNull
    @Size(max = 10)
    private Integer id;

    @NotNull
    @Nationalized
    private String name;
}
