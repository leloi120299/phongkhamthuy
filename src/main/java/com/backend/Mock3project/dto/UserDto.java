package com.backend.Mock3project.dto;

import com.backend.Mock3project.entity.Branch;
import com.backend.Mock3project.entity.Role;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserDto {
    private String id;
    private String name;
    private String sex;
    private Date birth;
    private String identityCard;
    private String phoneNumber;
    private String address;
    private String specialization;
    private String experience;
    private String username;
    private String email;
    private List<Role> roles;
    private Branch branch;
    private String note;
}
