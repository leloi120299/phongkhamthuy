package com.backend.Mock3project.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class NotificationRequest {
	@NotNull(message = "title must not be null")
    private String title;

	@NotNull(message = "message must not be null")
    private String message;

	@NotNull(message = "topic must not be null")
    private String topic;

    @NotNull(message = "date must not be null")
    private String dateString;

    private String token;
}