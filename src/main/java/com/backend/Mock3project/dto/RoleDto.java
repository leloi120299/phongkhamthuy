package com.backend.Mock3project.dto;

import lombok.Data;

@Data
public class RoleDto {
    private Integer id;
    private String name;
}
