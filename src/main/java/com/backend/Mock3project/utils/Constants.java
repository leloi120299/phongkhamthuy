package com.backend.Mock3project.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Constants {
    //regex
    public static final String REGEX_EMAIL = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    public static final String REGEX_PASSWORD = "^(?=.*?[0-9])(?=.*?[A-Z])(?=.*?[#?!@$%^&*\\-_]).{8,}$";
    public static final String REGEX_CMTND = "[0-9]{9,12}";
    public static final String REGEX_PHONENUMBER = "(^$|[0-9]{10,11})";

    //common links
    public static final String LINK_API = "/api/v1";
    public static final String LINK_ID = "/{id}";
    public static final String LINK_IMG = "http://10.10.104.214:8083/imgs/";

    //filter name, link
    public static final String BRANCH_DASHBOARD = "branch-filtering-dashboard";
}
