package com.backend.Mock3project.generator;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.util.stream.Stream;

public class SpeciesGenerator implements IdentifierGenerator {
    private final String speciesIdPrefix = "G";
    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object obj) throws HibernateException {
        String query = "SELECT s.id FROM Species s";
        Stream<String> ids = session.createQuery(query, String.class).stream();
        long max = ids.map(o -> o.replace(speciesIdPrefix, "")).mapToLong(Long::parseLong).max().orElse(0L);
        return speciesIdPrefix + (String.format("%05d", max + 1));
    }
}
