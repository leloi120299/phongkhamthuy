package com.backend.Mock3project.generator;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.util.stream.Stream;

public class PetGenerator implements IdentifierGenerator {
    private String prefix = "TN";

    /**
     * generate pet string id
     * @SharedSessionContractImplementor session
     * @Object obj
     * return String
     */
    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object obj) throws HibernateException {
        String query = "SELECT p.id FROM Pet p";
        Stream<String> ids = session.createQuery(query, String.class).stream();
        Long max = ids.map(o -> o.replace(prefix, "")).mapToLong(Long::parseLong).max().orElse(0L);
        return prefix + (String.format("%05d", max + 1));
    }
}
