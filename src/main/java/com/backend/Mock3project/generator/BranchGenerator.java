package com.backend.Mock3project.generator;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.util.stream.Stream;

public class BranchGenerator implements IdentifierGenerator {

    private final String branchIdPrefix = "CN";
    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object obj) throws HibernateException {
        String query = "SELECT b.id FROM Branch b";
        Stream<String> ids = session.createQuery(query, String.class).stream();
        long max = ids.map(o -> o.replace(branchIdPrefix, "")).mapToLong(Long::parseLong).max().orElse(0L);
        return branchIdPrefix + (String.format("%05d", max + 1));
    }
}
