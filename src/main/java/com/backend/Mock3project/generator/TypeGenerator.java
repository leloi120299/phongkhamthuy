package com.backend.Mock3project.generator;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.util.stream.Stream;

public class TypeGenerator implements IdentifierGenerator{
    private final String typeIdPrefix = "L";
    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object obj) throws HibernateException {
        String query = "SELECT t.id FROM Type t";
        Stream<String> ids = session.createQuery(query, String.class).stream();
        long max = ids.map(o -> o.replace(typeIdPrefix, "")).mapToLong(Long::parseLong).max().orElse(0L);
        return typeIdPrefix + (String.format("%05d", max + 1));
    }
}
