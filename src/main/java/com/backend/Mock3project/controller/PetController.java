package com.backend.Mock3project.controller;

import com.backend.Mock3project.dto.PetDto;
import com.backend.Mock3project.dto.PetRequestDto;
import com.backend.Mock3project.response.Response;
import com.backend.Mock3project.service.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;

import static com.backend.Mock3project.utils.Constants.LINK_API;
import static com.backend.Mock3project.utils.Constants.LINK_ID;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@RequestMapping(LINK_API + "/pet")
public class PetController {
    @Autowired
    PetService petService;

    /**
     * Get all Pet
     * @RequestParam page
     * @RequestParam size
     * @Return ResponseEntity<Response> with List<PetDto>
     */
    @GetMapping
    public ResponseEntity<Response> getAllPet(@RequestParam int page, @RequestParam int size) {
        Page<PetDto> petDtos = petService.getAllPet(page, size);
        return ResponseEntity.ok(
                Response.builder().data(petDtos.getContent())
                        .message("SUCCESS").status(OK.value()).total(petDtos.getTotalElements()).build()
        );
    }

    /**
     * Search Pet by name
     * @RequestParam page
     * @RequestParam size
     * @RequestParam name
     * @Return ResponseEntity<Response> with List<PetDto>
     */
    @GetMapping("/search")
    public ResponseEntity<Response> searchPet(@RequestParam int page, @RequestParam int size, @RequestParam String name) {
        Page<PetDto> petDtos = petService.searchPet(page, size, name);
        return ResponseEntity.ok(
                Response.builder().data(petDtos.getContent())
                        .message("SUCCESS").status(OK.value()).total(petDtos.getTotalElements()).build()
        );
    }

    /**
     * Get Pet by id
     * @PathVariable id
     * @Return ResponseEntity<Response> with PetDto object
     */
    @GetMapping(LINK_ID)
    public ResponseEntity<Response> searchPetById(@PathVariable String id) {
        PetDto petDto = petService.searchPetById(id);
        if (petDto == null) {
            return ResponseEntity.ok(Response.builder().message("Pet not found").status(NOT_FOUND.value()).build());
        }
        return ResponseEntity.ok(
                Response.builder().data(Collections.singletonList(petDto))
                        .message("SUCCESS").status(OK.value()).build());
    }

    /**
     * Add Pet
     * @RequestBody PetRequestDto
     * @Return ResponseEntity<Response> with message and status
     */
    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Response> addPet(@ModelAttribute @Valid PetRequestDto petRequestDto) {
        try {
            petService.savePet(petRequestDto);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(BAD_REQUEST.value()).build());
        }
    }

    /**
     * Update Pet by id
     * @PathVariable id
     * @RequestBody PetRequestDto
     * @Return ResponseEntity<Response> with message and status
     */
    @PutMapping(value = "/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Response> updatePet(@PathVariable String id, @ModelAttribute @Valid PetRequestDto petRequestDto) {
        try {
            petService.updatePet(id, petRequestDto);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(NOT_FOUND.value()).build());
        }
    }

    /**
     * Delete Pet by id
     * @PathVariable id
     * @Return ResponseEntity<Response> with message and status
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePet(@PathVariable String id) {
        try {
            petService.deletePet(id);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(BAD_REQUEST.value()).build());
        }
    }
}
