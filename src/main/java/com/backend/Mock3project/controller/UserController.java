package com.backend.Mock3project.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.backend.Mock3project.dto.UserDto;
import com.backend.Mock3project.dto.UserRequestDto;
import com.backend.Mock3project.entity.User;
import com.backend.Mock3project.response.Response;
import com.backend.Mock3project.security.TokenGenerate;
import com.backend.Mock3project.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.backend.Mock3project.utils.Constants.LINK_API;
import static com.backend.Mock3project.utils.Constants.LINK_ID;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_MODIFIED;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@RestController
@RequestMapping(LINK_API + "/user")
public class UserController {
    @Autowired
    UserService userService;

    /**
     * Get all User
     * @RequestParam page
     * @RequestParam size
     * @Return ResponseEntity<Response> with list<UserDto>
     */
    @GetMapping
    public ResponseEntity<Response> getAllUser(@RequestParam int page, @RequestParam int size) {
        Page<UserDto> users = userService.getAllUser(page, size);
        return ResponseEntity.ok(
                Response.builder().data(users.getContent())
                        .message("SUCCESS").status(OK.value()).total(users.getTotalElements()).build());
    }

    /**
     * Search User by name
     * @RequestParam page
     * @RequestParam size
     * @RequestParam name
     * @RequestParam phone
     * @RequestParam role
     * @RequestParam branch
     * @Return ResponseEntity<Response> with list<UserDto>
     */
    @GetMapping("/search")
    public ResponseEntity<Response> searchUser(@RequestParam int page, @RequestParam int size, @RequestParam String name, @RequestParam String phone, @RequestParam String role, @RequestParam String branch) {
        Page<UserDto> users = userService.searchUser(page, size, name, phone, role, branch);
        return ResponseEntity.ok(
                Response.builder().data(users.getContent())
                        .message("SUCCESS").status(OK.value()).total(users.getTotalElements()).build());
    }

    /**
     * Get User by id
     * @PathVariable id
     * @Return ResponseEntity<Response> with UserDto object
     */
    @GetMapping(LINK_ID)
    public ResponseEntity<Response> searchUserById(@PathVariable String id) {
        try {
            UserDto userDto = userService.searchById(id);
            return ResponseEntity.ok(
                    Response.builder().data(Collections.singletonList(userDto))
                            .message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(NOT_FOUND.value()).build());
        }
    }

    /**
     * Add User
     * @RequestBody UserRequestDto
     * @Return ResponseEntity<Response> with message and status
     */
    @PostMapping
    public ResponseEntity<Response> saveUser(@RequestBody @Valid UserRequestDto userRequestDto) {
        try {
            userService.saveUser(userRequestDto);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(BAD_REQUEST.value()).build());
        }
    }

    /**
     * Update User by id
     * @PathVariable id
     * @RequestBody UserRequestDto
     * @Return ResponseEntity<Response> with message and status
     */
    @PutMapping("/{id}")
    public ResponseEntity<Response> updateUser(@PathVariable String id, @RequestBody @Valid UserRequestDto userRequestDto) {
        try {
            userService.updateUser(id, userRequestDto);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(NOT_FOUND.value()).build());
        }
    }

    /**
     * Delete User by id
     * @PathVariable id
     * @Return ResponseEntity<Response> with message and status
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable String id) {
        try {
            userService.deleteUser(id);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(BAD_REQUEST.value()).build());
        }
    }

    /**
     * Change User password by id
     * @RequestParam username
     * @RequestParam oldPassword
     * @RequestParam newPassword
     * @Return ResponseEntity<Response> with message and status
     */
    @PutMapping("/change")
    public ResponseEntity<?> changePassword(@RequestParam String username,@RequestParam String oldPassword,@RequestParam String newPassword) {
        try {
            userService.changePassword(username, oldPassword, newPassword);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(NOT_MODIFIED.value()).build());
        }
    }

    /**
     * Forgot User password by id
     * @RequestParam username
     * @RequestParam oldPassword
     * @RequestParam newPassword
     * @Return ResponseEntity<Response> with message and status
     */
    @PutMapping("/forgot")
    public ResponseEntity<?> forgotPassword(@RequestParam String username,@RequestParam String newPassword,@RequestParam String email) {
        try {
            userService.forgotPassword(username, newPassword, email);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(NOT_MODIFIED.value()).build());
        }
    }

    /**
     * Refresh token
     * @HttpServletRequest request
     * @HttpServletResponse response
     * @Return ResponseEntity<Response> with message and status
     * @throws IOException
     */
    @GetMapping("/token/refresh")
    public ResponseEntity<?> refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {       //send request with a token se co chu bearer de cho thay rang  dang gui request co chua token
            try {
                String refresh_token = authorizationHeader.substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(refresh_token); //giai ma token
                String username = decodedJWT.getSubject();
                User user = userService.getUser(username);
                String access_token = TokenGenerate.generateAccessToken(null, user, request);
                Map<String, Object> tokenReturn = new HashMap<>();
                tokenReturn.put("access_token", access_token);
                return ResponseEntity.ok(Response.builder().data(Collections.singletonList(tokenReturn)).message("SUCCESS").status(OK.value()).build());
            } catch (Exception e) {
                response.setHeader("error", e.getMessage());
                response.setStatus(FORBIDDEN.value());
                Map<String, String> error = new HashMap<>();
                error.put("error_message", e.getMessage());
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        } else {
            throw new RuntimeException("Refresh token is missing");
        }
        return null;
    }
}
