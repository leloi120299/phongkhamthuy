package com.backend.Mock3project.controller;

import com.backend.Mock3project.dto.TypeDto;
import com.backend.Mock3project.dto.TypeRequestDto;
import com.backend.Mock3project.response.Response;
import com.backend.Mock3project.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;

import static com.backend.Mock3project.utils.Constants.LINK_API;
import static com.backend.Mock3project.utils.Constants.LINK_ID;
import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(LINK_API + "/type")
public class TypeController {
    @Autowired
    TypeService typeService;

    /**
     * Get all Type
     * @Requestparam page
     * @Requestparam size
     * @return ResponseEntity<Response> with list<TypeDto>
     */
    @GetMapping("")
    public ResponseEntity<Response> getListType(@RequestParam(name = "page") int pageNum,
                                                @RequestParam(name = "size") int pageSize) {
        Page<TypeDto> typeDtos = typeService.getListType(pageNum, pageSize);
        return ResponseEntity.ok(
                Response.builder().data(typeDtos.getContent())
                        .message("SUCCESS").status(OK.value()).total(typeDtos.getTotalElements()).build());
    }

    /**
     * Get Type by id
     * @PathVariable id
     * @return ResponseEntity<Response> with single TypeDto object
     */
    @GetMapping(LINK_ID)
    public ResponseEntity<Response> searchTypeById(@PathVariable String id) {
        try {
            TypeDto typeDto = typeService.searchByTypeId(id);
            return ResponseEntity.ok(
                    Response.builder().data(Collections.singletonList(typeDto))
                            .message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(NOT_FOUND.value()).build());
        }
    }

    /**
     * Search Type by Name
     * @RequestParam name
     * @RequestParam pageNum
     * @RequestParam pageSize
     * @return ResponseEntity<Response> with list<TypeDto>
     */
    @GetMapping("/search")
    public ResponseEntity<Response> searchTypeByName(@RequestParam("name") String name,
                                                 @RequestParam(name = "page") int pageNum,
                                                 @RequestParam(name = "size") int pageSize) {
        Page<TypeDto> typeDtos = typeService.searchByTypeName(name, pageNum, pageSize);
        return ResponseEntity.ok(
                Response.builder().data(typeDtos.getContent())
                        .message("SUCCESS").status(OK.value()).total(typeDtos.getTotalElements()).build());
    }

    /**
     * Add new Type
     * @RequestBody typeRequestDto
     * @return status and message
     */
    @PostMapping
    public ResponseEntity<?> addType (@RequestBody @Valid TypeRequestDto typeRequestDto) {
        try {
            typeService.saveType(typeRequestDto);
            return ResponseEntity.ok(
                    Response.builder()
                            .message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(BAD_REQUEST.value()).build());
        }
    }

    /**
     * Update Type by Id
     * @PathVariable id
     * @RequestBody typeRequestDto
     * @return message and status
     */
    @PutMapping(LINK_ID)
    public ResponseEntity<?> updateType(@PathVariable String id,
                                               @RequestBody @Valid TypeRequestDto typeRequestDto) {
        try {
            typeService.updateType(id, typeRequestDto);
            return ResponseEntity.ok(
                    Response.builder()
                            .message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(BAD_REQUEST.value()).build());
        }
    }

    /**
     * Delete Type by id
     * @PathVariable id
     * @return message and status
     */
    @DeleteMapping(LINK_ID)
    public ResponseEntity<?> deleteType(@PathVariable String id) {
        try {
            typeService.deleteType(id);
            return ResponseEntity.ok(
                    Response.builder()
                            .message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(BAD_REQUEST.value()).build());
        }
    }
}
