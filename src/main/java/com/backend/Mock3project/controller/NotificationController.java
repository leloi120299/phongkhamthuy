package com.backend.Mock3project.controller;

import com.backend.Mock3project.dto.NotificationRequest;
import com.backend.Mock3project.dto.UserDeviceRequestDto;
import com.backend.Mock3project.dto.UserNotificationDto;
import com.backend.Mock3project.response.Response;
import com.backend.Mock3project.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static com.backend.Mock3project.utils.Constants.LINK_API;
import static org.springframework.http.HttpStatus.NOT_MODIFIED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping(LINK_API + "/notification")
public class NotificationController {
    @Autowired
    NotificationService notificationService;

    /**
     * send notification
     *
     * @RequestBody NotificationRequest
     * @Return ResponseEntity<Response> with message and status
     */
    @PostMapping
    public ResponseEntity<Response> sendNotification(@RequestParam String userId, @RequestBody @Valid NotificationRequest request) {
        try {
            notificationService.sendNotificationToToken(userId, request);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(NOT_MODIFIED.value()).build());
        }
    }

    /**
     * Add user device
     * @RequestBody UserDeviceRequestDto
     * @Return ResponseEntity<Response> with message and status
     */
    @PostMapping("/device")
    public ResponseEntity<Response> addUserDevice(@RequestBody @Valid UserDeviceRequestDto userDeviceRequestDto) {
        try {
            notificationService.addUserDevice(userDeviceRequestDto);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(NOT_MODIFIED.value()).build());
        }
    }

    /**
     * Logout user device
     * @RequestParam String token
     * @Return ResponseEntity<Response> with message and status
     */
    @PutMapping
    public ResponseEntity<Response> deactivateUserDevice(@RequestParam String token) {
        Integer status = notificationService.deactivateUserDevice(token);
        if (status == 0) {
            return ResponseEntity.ok(Response.builder().message("Token not exist").status(NOT_MODIFIED.value()).build());
        }
        return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
    }

    /**
     * Get all user notification
     * @RequestParam page
     * @RequestParam size
     * @RequestParam userId
     * @Return ResponseEntity<Response> with list<UserNotificationDto>
     */
    @GetMapping
    public ResponseEntity<Response> getAllNotification(@RequestParam int page, @RequestParam int size, @RequestParam String userId) {
        Page<UserNotificationDto> userNotificationDtos = notificationService.getAllNotificationByUser(page, size, userId);
        if (userNotificationDtos == null) {
            return ResponseEntity.ok(
                    Response.builder()
                            .message("User id not exist").status(NOT_FOUND.value()).build());
        }
        return ResponseEntity.ok(
                Response.builder().data(userNotificationDtos.getContent())
                        .message("SUCCESS").status(OK.value()).total(userNotificationDtos.getTotalElements()).build()
        );
    }

}