package com.backend.Mock3project.controller;


import com.backend.Mock3project.dto.SpeciesDto;
import com.backend.Mock3project.dto.SpeciesRequestDto;
import com.backend.Mock3project.response.Response;
import com.backend.Mock3project.service.SpeciesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;

import static com.backend.Mock3project.utils.Constants.LINK_API;
import static com.backend.Mock3project.utils.Constants.LINK_ID;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(LINK_API + "/species")
public class SpeciesController {
    @Autowired
    SpeciesService speciesService;
    /**
     * Get all Species
     * @RequestParam page
     * @RequestParam size
     * @Return ResponseEntity<Response> with list<SpeciesDto>
     */
    @GetMapping("")
    public ResponseEntity<Response> getAllSpecies(@RequestParam(name = "page") int page,
                                                  @RequestParam(name = "size") int size) {
        Page<SpeciesDto> speciesDtos = speciesService.getSpecies(page, size);
        return ResponseEntity.ok(
                Response.builder().data(speciesDtos.getContent())
                        .message("SUCCESS").status(OK.value())
                        .total(speciesDtos.getTotalElements())
                        .build()
        );
    }
    /**
     * Get Species by id
     * @PathVariable id
     * @Return ResponseEntity<Response> with single SpeciesDto object
     */
    @GetMapping(LINK_ID)
    public ResponseEntity<Response> getSpeciesById(@PathVariable String id) {
        try {
            return ResponseEntity.ok(Response.builder().data(Collections.singletonList(speciesService
                    .getSpeciesById(id))).message("SUCCESS").status(200).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(400).build());
        }
    }

    /**
     * addSpecies
     * @RequestBody speciesRequestDto
     * @Return ResponseEntity<Response> with message and status
     */
    @PostMapping
    public ResponseEntity<?> addSpecies(@RequestBody @Valid SpeciesRequestDto speciesRequestDto) {
        try {
            speciesService.saveSpecies(speciesRequestDto);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(200).build());
        } catch (Exception exception) {
            return ResponseEntity.ok(Response.builder().message(exception.getMessage()).status(400).build());
        }
    }
    /**
     * Delete Species by id
     * @PathVariable id
     * @Return ResponseEntity<Response> with message and status
     */
    @DeleteMapping(LINK_ID)
    //must have build in return responseentity
    public ResponseEntity<?> deleteSpecies(@PathVariable String id) {
        try {
            speciesService.deleteSpecies(id);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(200).build());
        } catch (Exception exception) {
            return ResponseEntity.ok(Response.builder().message(exception.getMessage()).status(400).build());
        }
    }
    /**
     * Update Species by id
     * @PathVariable id
     * @RequestBody speciesRequestDto
     * @Return ResponseEntity<Response> with message and status
     */
    @PutMapping(LINK_ID)
    //must have build in return responseentity
    public ResponseEntity<Response> updateSpecies(@PathVariable String id, @RequestBody @Valid SpeciesRequestDto speciesRequestDto) {
        try {
            speciesService.updateSpecies(id, speciesRequestDto);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(200).build());
        } catch (Exception exception) {
            return ResponseEntity.ok(Response.builder().message(exception.getMessage()).status(400).build());
        }
    }

    /**
     * Search Species by name,address,phone
     * @Param name
     * @Param address
     * @Param phone
     * @RequestParam page
     * @RequestParam size
     * @Return ResponseEntity<Response> with list<SpeciesDto>
     */
    @GetMapping("/search")
    //must have build in return responseentity
    public ResponseEntity<Response> searchByName(@Param("name") String name, @RequestParam(name = "page") int page,
                                                 @RequestParam(name = "size") int size) {
        try {
            Page<SpeciesDto> speciesDtos = speciesService.searchBySpeciesName(name, page, size);
            return ResponseEntity.ok(Response.builder().data(speciesDtos.getContent()).message("SUCCESS")
                    .status(200)
                    .total(speciesDtos.getTotalElements())
                    .build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(400).build());
        }
    }
}
