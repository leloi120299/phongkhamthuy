package com.backend.Mock3project.controller;

import com.backend.Mock3project.dto.ScheduleDto;
import com.backend.Mock3project.dto.ScheduleRequestDto;
import com.backend.Mock3project.entity.Schedule;
import com.backend.Mock3project.response.Response;
import com.backend.Mock3project.service.ScheduleService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.backend.Mock3project.utils.Constants.LINK_API;
import static com.backend.Mock3project.utils.Constants.LINK_ID;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(LINK_API + "/schedule")
public class ScheduleController {
    @Autowired
    private ScheduleService scheduleService;

    /**
     * Get all Schedule
     * @RequestParam page
     * @RequestParam size
     * @return ResponseEntity<Response> with message, status, total and list<ScheduleDto>
     */
    @GetMapping
    public ResponseEntity<Response> getAllSchedule(@RequestParam int page, @RequestParam int size) {
        List<ScheduleDto> scheduleDtos = scheduleService.getAllSchedule(page, size);
        return ResponseEntity.ok(
                Response.builder().data(scheduleDtos)
                        .message("SUCCESS").status(OK.value()).total((long) scheduleService.countSchedules()).build()
        );
    }

    /**
     * Search schedule by name of customer
     * @RequestParam page
     * @RequestParam size
     * @RequestParam name
     * @return ResponseEntity<Response> with message, status, total and list<ScheduleDto>
     */
    @GetMapping("/search")
    public ResponseEntity<Response> searchScheduleByNameCustomer(@RequestParam int page,
                                                                 @RequestParam int size,
                                                                 @RequestParam String name) {
        List<ScheduleDto> scheduleDtos = scheduleService.searchScheduleByNameCustomer(page, size, name);
        return ResponseEntity.ok(
                Response.builder().data(scheduleDtos)
                        .message("SUCCESS").status(OK.value()).total((long) scheduleService.countScheduleByNameCustomer(name)).build()
        );
    }

    /**
     * Search schedule by date and time
     * @RequestParam page
     * @RequestParam size
     * @RequestParam date
     * @RequestParam time
     * @return ResponseEntity<Response> with message, status, total and list<ScheduleDto>
     */
    @GetMapping("/searchDateTime")
    public ResponseEntity<Response> findByDateAndTime(@RequestParam int page,
                                                      @RequestParam int size,
                                                      @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy", iso = DateTimeFormat.ISO.DATE) LocalDate date,
                                                      @RequestParam @DateTimeFormat(pattern = "HH:mm", iso = DateTimeFormat.ISO.TIME) LocalTime time){
        List<ScheduleDto> scheduleDtos = scheduleService.findByDateAndTime(page, size, date, time);
        return ResponseEntity.ok(
                Response.builder().data(scheduleDtos)
                        .message("SUCCESS").status(OK.value()).total((long) scheduleService.countByDateAndTime(date, time)).build()
        );
    }

    /**
     * Search schedule by interval date
     * @RequestParam page
     * @RequestParam size
     * @RequestParam date1
     * @RequestParam date2
     * @return ResponseEntity<Response> with message, status, total and list<ScheduleDto>
     */
    @GetMapping("/date")
    public ResponseEntity<Response> findSchedulesByDateInterval(@RequestParam int page,
                                                                @RequestParam int size,
                                                                @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy", iso = DateTimeFormat.ISO.DATE) Date date1,
                                                                @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy", iso = DateTimeFormat.ISO.DATE) Date date2){
        List<ScheduleDto> scheduleDtos = scheduleService.findSchedulesByDateInterval(page, size, date1, date2);
        return ResponseEntity.ok(
                Response.builder().data(scheduleDtos)
                        .message("SUCCESS").status(OK.value()).total((long) scheduleService.countSchedulesByDateInterval(date1, date2)).build()
        );
    }

    /**
     * Search schedule by id
     * @PathVariable id
     * @return ResponseEntity<Response> with message and status
     * @throws NotFoundException
     */
    @GetMapping(LINK_ID)
    public ResponseEntity<Response> findScheduleById(@PathVariable("id") Integer id) throws NotFoundException{
        try {
            ScheduleDto scheduleDto = scheduleService.findScheduleById(id);
            return ResponseEntity.ok(
                    Response.builder().message("SUCCESS").status(200).data(Collections.singletonList(scheduleDto)).build()
            );
        }catch (NotFoundException exception) {
            return ResponseEntity.ok(Response.builder().message(exception.getMessage()).status(400).build());
        }
    }

    /**
     * Search schedule by branch id
     * @RequestParam page
     * @RequestParam size
     * @RequestParam id
     * @return ResponseEntity<Response> with message, status, total and list<ScheduleDto>
     */
    @GetMapping("/branch")
    public ResponseEntity<Response> findSchedulesByBranchId(@RequestParam int page,
                                                            @RequestParam int size,
                                                            @RequestParam String id) {
        List<ScheduleDto> scheduleDtos = scheduleService.findSchedulesByBranchId(page, size, id);
        return ResponseEntity.ok(
                Response.builder().data(scheduleDtos)
                        .message("SUCCESS").status(OK.value()).total((long) scheduleService.countSchedulesByBranchId(id)).build()
        );
    }

    /**
     * Search schedule by doc id
     * @RequestParam page
     * @RequestParam size
     * @RequestParam id
     * @return ResponseEntity<Response> with message, status, total and list<ScheduleDto>
     */
    @GetMapping("/doctor")
    public ResponseEntity<Response> findSchedulesByDocId(@RequestParam int page,
                                                         @RequestParam int size,
                                                         @RequestParam String id) {
        List<ScheduleDto> scheduleDtos = scheduleService.findSchedulesByDocId(page, size, id);
        return ResponseEntity.ok(
                Response.builder().data(scheduleDtos)
                        .message("SUCCESS").status(OK.value()).total((long) scheduleService.countSchedulesByDocId(id)).build()
        );
    }

    /**
     * Count schedule by status
     * @RequestParam status
     * @return ResponseEntity<Response> with message, status, total
     * @throws NotFoundException
     */
    @GetMapping("/status")
    public ResponseEntity<Response> countScheduleByStatus(@RequestParam(name = "status") String status) throws NotFoundException {
        try {
            int count = scheduleService.countScheduleByStatus(status);
            return ResponseEntity.ok(
                    Response.builder().message("SUCCESS").status(200).total((long) count).build()
            );
        }catch (NotFoundException exception) {
            return ResponseEntity.ok(Response.builder().message(exception.getMessage()).status(400).build());
        }
    }

    /**
     * Save schedule
     * @RequestBody scheduleRequestDto
     * @return ResponseEntity<Response> with message and status
     */
    @PostMapping
    public ResponseEntity<Response> saveSchedule(@RequestBody @Valid ScheduleRequestDto scheduleRequestDto) {
        Schedule schedule = scheduleService.saveSchedule(scheduleRequestDto);
        if (schedule == null) {
            return ResponseEntity.ok(Response.builder().message("Doc id not exist").status(NO_CONTENT.value()).build());
        }
        return ResponseEntity.ok(
                Response.builder().data(Collections.singletonList(schedule)).message("SUCCESS").status(200).build());
    }

    /**
     * Delete schedule by id
     * @PathVariable id
     * @return ResponseEntity<Response> with message and status
     * @throws NotFoundException
     */
    @DeleteMapping(LINK_ID)
    public ResponseEntity<Response> deleteScheduleById(@PathVariable Integer id) throws NotFoundException {
        try {
            scheduleService.deleteScheduleById(id);
            return ResponseEntity.ok(
                    Response.builder().message("SUCCESS").status(200).build()
            );
        } catch (NotFoundException exception) {
            return ResponseEntity.ok(Response.builder().message(exception.getMessage()).status(400).build());
        }
    }

    /**
     * Update schedule
     * @PathVariable id
     * @RequestBody scheduleRequestDto
     * @return ResponseEntity<Response> with message and status
     * @throws NotFoundException
     */
    @PutMapping(LINK_ID)
    public ResponseEntity<Response> updateSchedule(@PathVariable Integer id,
                                                   @RequestBody @Valid ScheduleRequestDto scheduleRequestDto) throws NotFoundException {
        if (scheduleService.updateSchedule(id, scheduleRequestDto) == null) {
            return ResponseEntity.ok(Response.builder().message("Doc id not exist").status(NO_CONTENT.value()).build());
        }
        return ResponseEntity.ok(
                Response.builder().message("SUCCESS").status(OK.value()).build());
    }
}
