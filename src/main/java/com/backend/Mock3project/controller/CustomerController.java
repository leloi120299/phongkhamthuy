package com.backend.Mock3project.controller;

import com.backend.Mock3project.dto.CustomerDto;
import com.backend.Mock3project.dto.CustomerRequestDto;
import com.backend.Mock3project.entity.Customer;
import com.backend.Mock3project.response.Response;
import com.backend.Mock3project.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;

import static com.backend.Mock3project.utils.Constants.LINK_API;
import static com.backend.Mock3project.utils.Constants.LINK_ID;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@RequestMapping(LINK_API + "/customer")
public class CustomerController {
    @Autowired
    CustomerService customerService;

    /**
     * Get all Customer
     * @RequestParam page
     * @RequestParam size
     * @Return ResponseEntity<Response> with list<CustomerDto>
     */
    @GetMapping
    public ResponseEntity<Response> getAllCustomer(@RequestParam int page, @RequestParam int size) {
        Page<CustomerDto> customerDtos = customerService.getAllCustomer(page, size);
        return ResponseEntity.ok(
                Response.builder().data(customerDtos.getContent())
                        .message("SUCCESS").status(OK.value()).total(customerDtos.getTotalElements()).build()
        );
    }

    /**
     * Search Customer by name,address,phone
     * @RequestParam page
     * @RequestParam size
     * @RequestParam name
     * @RequestParam branch
     * @Return ResponseEntity<Response> with list<BranchDto>
     */
    @GetMapping("/search")
    public ResponseEntity<Response> searchCustomer(@RequestParam int page, @RequestParam int size, @RequestParam String name, @RequestParam String branch) {
        Page<CustomerDto> customerDtos = customerService.searchCustomer(page, size, name, branch);
        return ResponseEntity.ok(
                Response.builder().data(customerDtos.getContent())
                        .message("SUCCESS").status(OK.value()).total(customerDtos.getTotalElements()).build()
        );
    }

    /**
     * Get Customer by id
     * @PathVariable id
     * @Return ResponseEntity<Response> with CustomerDto object
     */
    @GetMapping(LINK_ID)
    public ResponseEntity<Response> searchCustomerById(@PathVariable String id) {
        CustomerDto customerDto = customerService.searchCustomerById(id);
        if (customerDto == null) {
            return ResponseEntity.ok(Response.builder().message("Customer not found").status(NOT_FOUND.value()).build());
        }
        return ResponseEntity.ok(
                Response.builder().data(Collections.singletonList(customerDto))
                        .message("SUCCESS").status(OK.value()).build());
    }

    /**
     * Add Customer
     * @RequestBody CustomerRequestDto
     * @Return ResponseEntity<Response> with message and status
     */
    @PostMapping
    public ResponseEntity<Response> addCustomer(@RequestBody @Valid CustomerRequestDto customerRequestDto) {
        customerService.saveCustomer(customerRequestDto);
        return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
    }

    /**
     * Update Customer by id
     * @PathVariable id
     * @RequestBody CustomerRequestDto
     * @Return ResponseEntity<Response> with message and status
     */
    @PutMapping("/{id}")
    public ResponseEntity<Response> updateCustomer(@PathVariable String id, @RequestBody @Valid CustomerRequestDto customerRequestDto) {
        Customer customer = customerService.updateCustomer(id, customerRequestDto);
        if (customer == null) {
            return ResponseEntity.ok(Response.builder().message("Customer not found").status(NOT_FOUND.value()).build());
        }
        return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
    }

    /**
     * Delete Customer by id
     * @PathVariable id
     * @Return ResponseEntity<Response> with message and status
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable String id) {
        try {
            customerService.deleteCustomer(id);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(OK.value()).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(BAD_REQUEST.value()).build());
        }
    }
}
