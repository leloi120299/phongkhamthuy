package com.backend.Mock3project.controller;


import com.backend.Mock3project.dto.BranchDto;
import com.backend.Mock3project.dto.BranchRequestDto;
import com.backend.Mock3project.response.Response;
import com.backend.Mock3project.service.BranchService;
import com.fasterxml.jackson.core.JsonProcessingException;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

import static com.backend.Mock3project.filtering.JacksonConfiguration.filterBranch;
import static com.backend.Mock3project.filtering.JacksonConfiguration.customFilteringProperties;
import static com.backend.Mock3project.utils.Constants.*;
import static org.springframework.http.HttpStatus.OK;


@RestController
@RequestMapping(LINK_API + "/branches")
public class BranchController {



    @Autowired
    BranchService branchService;
    /**
     * Get all Branches
     * @RequestParam page
     * @RequestParam size
     * @Return ResponseEntity<Response> with list<BranchDto>
     * @throws JsonProcessingException -- exception
     */
    @GetMapping("")
    public ResponseEntity<Response> getAllBranch(@RequestParam(name = "page") int page,
                                                 @RequestParam(name = "size") int size) throws JsonProcessingException {
        Page<BranchDto> branchDtos = branchService.getBranches(page, size);
        return ResponseEntity.ok(
                Response.builder().data((List<?>) setFilteringProperties(branchDtos.getContent()))
                        .total(branchDtos.getTotalElements())
                        .message("SUCCESS").status(OK.value()).build());
    }


    /**
     * Get Branches by id
     * @PathVariable id
     * @Return ResponseEntity<Response> with single BranchDto object
     */
    @GetMapping(LINK_ID)
    public ResponseEntity<Response> getBranchById(@PathVariable String id) {
        try {
            return ResponseEntity.ok(Response.builder().data(Collections.singletonList(setFilteringProperties(
                    singleBranchDtoById(id)))).message("SUCCESS").status(200).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(404).build());
        }
    }

    /**
     * addBranch
     * @RequestBody branchRequestDto
     * @Return ResponseEntity<Response> with message and status
     */
    @PostMapping
    public ResponseEntity<?> addBranch(@RequestBody @Valid BranchRequestDto branchRequestDto) {
        try {
            branchService.saveBranch(branchRequestDto);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(200).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(400).build());
        }
    }
    /**
     * Delete Branches by id
     * @PathVariable id
     * @Return ResponseEntity<Response> with message and status
     */
    @DeleteMapping(LINK_ID)
    //must have build in return responseentity
    public ResponseEntity<?> deleteBranch(@PathVariable String id) {
        try {
            branchService.deleteBranch(id);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(200).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(400).build());
        }
    }
    /**
     * Update Branches by id
     * @PathVariable id
     * @RequestBody branchRequestDto
     * @Return ResponseEntity<Response> with message and status
     */
    @PutMapping(LINK_ID)
    //must have build in return responseentity
    public ResponseEntity<Response> updateBranch(@PathVariable String id, @RequestBody @Valid BranchRequestDto branchRequestDto) {
        try {
            branchService.updateBranch(id, branchRequestDto);
            return ResponseEntity.ok(Response.builder().message("SUCCESS").status(200).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(400).build());
        }
    }
    /**
     * Search Branches by name,address,phone
     * @Param name
     * @Param address
     * @Param phone
     * @RequestParam page
     * @RequestParam size
     * @Return ResponseEntity<Response> with list<BranchDto>
     */
    @GetMapping("/search")
    //must have build in return responseentity
    public ResponseEntity<Response> searchByName(@Param("name") String name, @Param("address") String address,
                                                 @Param("phone") String phone, @RequestParam(name = "page") int page,
                                                 @RequestParam(name = "size") int size) {
        try {
            Page<BranchDto> branchDtos = branchService.searchBranch(name, address, phone, page, size);
            return ResponseEntity.ok(Response.builder().data((List<?>) setFilteringProperties(branchDtos.getContent())).message("SUCCESS")
                    .status(200)
                    .total(branchDtos.getTotalElements())
                    .build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(400).build());
        }
    }
    /**
     * Update Branches by id
     * @PathVariable id
     * @Return ResponseEntity<Response> with single BranchDto object
     */
    @GetMapping("/filtering-dashboard" + LINK_ID)
    public ResponseEntity<?> getBranchForDashboard(@PathVariable String id) {
        try {
            return ResponseEntity.ok(Response.builder().data(Collections.singletonList(singleBranchDtoById(id))
            ).message("SUCCESS").status(200).build());
        } catch (Exception e) {
            return ResponseEntity.ok(Response.builder().message(e.getMessage()).status(404).build());
        }
    }
    /**
     * setFilteringProperties for Object
     * @Return Object after filtering
     * @throws JsonProcessingException -- exception
     */
    public Object setFilteringProperties(Object object) throws JsonProcessingException {
        return customFilteringProperties(object, BRANCH_DASHBOARD, filterBranch);
    }
    /**
     * get singleBranchDtoById for BranchDto
     * @Return BranchDto
     */
    public BranchDto singleBranchDtoById(String id) throws NotFoundException {
        return branchService.getBranchById(id);
    }


}
