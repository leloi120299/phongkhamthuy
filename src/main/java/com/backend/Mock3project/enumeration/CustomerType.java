package com.backend.Mock3project.enumeration;

import lombok.Getter;

@Getter
public enum CustomerType {
    NORMAL(1, "normal"), VIP(2, "vip");
    private final int code;
    private final String text;

    CustomerType(int code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * get customer type by number
     * @int code
     * return String
     */
    public static String getTextByCode(int code) {
        for (CustomerType customerType : CustomerType.values()) {
            if (customerType.code == code) {
                return customerType.text;
            }
        }
        return null;
    }
}
