package com.backend.Mock3project.enumeration;

import lombok.Getter;

@Getter
public enum EmployeeType {
    DOC(1, "DOC"), EMP(2, "EMP");
    private final int code;
    private final String text;

    EmployeeType(int code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * get employee type by number
     * @int code
     * return String
     */
    public static String getTextByCode(int code) {
        for (EmployeeType employeeType : EmployeeType.values()) {
            if (employeeType.code == code) {
                return employeeType.text;
            }
        }
        return null;
    }
}
