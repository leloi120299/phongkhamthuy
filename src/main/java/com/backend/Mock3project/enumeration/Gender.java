package com.backend.Mock3project.enumeration;

import lombok.Getter;

@Getter
public enum Gender {
    MALE(1, "male"), FEMALE(2, "female");
    private final int code;
    private final String text;

    Gender(int code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * get gender type by number
     * @int code
     * return String
     */
    public static String getTextByCode(int code) {
        for (Gender gender : Gender.values()) {
            if (gender.code == code) {
                return gender.text;
            }
        }
        return null;
    }
}
