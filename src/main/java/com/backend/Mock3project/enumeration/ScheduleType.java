package com.backend.Mock3project.enumeration;

import lombok.Getter;

@Getter
public enum ScheduleType {
    SUCCESS(1, "success"),
    PROCESS(2, "process"),
    CANCEL(3, "cancel");
    private final int code;
    private final String text;

    ScheduleType(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public static String getTextByCode(int code) {
        for (ScheduleType scheduleType : ScheduleType.values()) {
            if (scheduleType.code == code) {
                return scheduleType.text;
            }
        }
        return null;
    }
}
