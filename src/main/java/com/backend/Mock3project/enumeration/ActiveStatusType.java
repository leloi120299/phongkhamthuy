package com.backend.Mock3project.enumeration;

import lombok.Getter;

@Getter
public enum ActiveStatusType {
    NORMAL(1, "login"), VIP(2, "logout");
    private final int code;
    private final String text;

    ActiveStatusType(int code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * get active status type by number
     * @int code
     * return String
     */
    public static String getTextByCode(int code) {
        for (ActiveStatusType activeStatusType : ActiveStatusType.values()) {
            if (activeStatusType.code == code) {
                return activeStatusType.text;
            }
        }
        return null;
    }
}
