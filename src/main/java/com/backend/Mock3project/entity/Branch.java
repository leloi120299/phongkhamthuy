package com.backend.Mock3project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
@JsonIgnoreProperties(value = {"customerNumber","petNumber","typeNumber","speciesNumber","userNumber"})
public class Branch{

    @Id
    @GeneratedValue(generator = "branch_generator")
    @GenericGenerator(name = "branch_generator", strategy = "com.backend.Mock3project.generator.BranchGenerator")
    @Column(name="id")
    private String id;

    @NotNull
    private String name;

    @NotNull
    private String address;

    @Column(name = "phone_number")
    @NotNull
    private String phoneNumber;

    private String note;

    @JsonIgnore
    private Integer flag;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "branch")
    private List<User> users;

    @ManyToMany(mappedBy = "branches")
    @JsonIgnore
    private List<Customer> customers;

    //only take * ,letter not working
    @Formula("(SELECT COUNT(*) FROM customer_branch cb\n" +
            "inner join customer c on c.id = cb.customer_id\n" +
            "where cb.branch_id = id and c.flag <> 1)")
    private int customerNumber;

    @Formula("(SELECT COUNT(*) FROM pet p INNER JOIN customer c on c.id = p.customer_id " +
            "INNER JOIN customer_branch cb on c.id = cb.customer_id WHERE  cb.branch_id = id AND p.flag <> 1)")
    private int petNumber;

    @Formula("(SELECT COUNT(*) FROM type t WHERE t.flag <> 1)")
    private int typeNumber;

    @Formula("(SELECT COUNT(*) FROM species s WHERE s.flag <> 1)")
    private int speciesNumber;

    @Formula("(SELECT COUNT(*) FROM mock3.public.user u WHERE u.branch_id = id AND u.flag <> 1)")
    private int userNumber;
}
