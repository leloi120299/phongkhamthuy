package com.backend.Mock3project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Nationalized;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
    @Id
    @GeneratedValue(generator = "customer_generator")
    @GenericGenerator(name = "customer_generator", strategy = "com.backend.Mock3project.generator.CustomerGenerator")
    private String id;

    @Nationalized
    @NotNull
    private String name;

    @Nationalized
    @Size(max = 500)
    @NotNull
    private String address;

    @Column(name = "phone_number")
    @NotNull
    private String phoneNumber;

    @NotNull
    private String email;

    @Nationalized
    @NotNull
    private String type;

    @OneToMany
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(
            name = "customer_id",
            referencedColumnName = "id",
            insertable = false,
            updatable = false,
            foreignKey = @javax.persistence.ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private List<Pet> pets;

    @JsonIgnore
    private Integer flag;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "customer_branch",
            joinColumns = {@JoinColumn(name = "customer_id")},
            inverseJoinColumns = {@JoinColumn(name = "branch_id")})
    private List<Branch> branches;

}
