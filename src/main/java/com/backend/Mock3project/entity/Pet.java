package com.backend.Mock3project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Pet {
    @Id
    @GeneratedValue(generator = "pet_generator")
    @GenericGenerator(name = "pet_generator", strategy = "com.backend.Mock3project.generator.PetGenerator")
    private String id;

    @Nationalized
    @NotNull
    private String name;

    @Nationalized
    @NotNull
    private String sex;

    @Column(name = "birth_certi")
    @NotNull
    private String birthCerti;

    @Nationalized
    @Size(max = 500)
    private String note;

    @OneToMany(mappedBy = "pet", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Schedule> schedules;

    @Column(name = "customer_id")
    private String customerId;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "pet_species",
            joinColumns = @JoinColumn(name = "pet_id"),
            inverseJoinColumns = @JoinColumn(name = "species_id")
    )
    private List<Species> species;

    @JsonIgnore
    private Integer flag;
}
