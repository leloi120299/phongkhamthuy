package com.backend.Mock3project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
public class Type {
    @Id
    @GeneratedValue(generator = "type_generator")
    @GenericGenerator(name = "type_generator", strategy = "com.backend.Mock3project.generator.TypeGenerator")
    private String id;

    @NotNull
    private String name;

    @Size(max = 500)
    private String note;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "type")
    List<Species> species;

    @JsonIgnore
    private Integer flag;
}
