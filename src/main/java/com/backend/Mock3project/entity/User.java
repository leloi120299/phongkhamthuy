package com.backend.Mock3project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "\"user\"")
public class User {
    @Id
    @GeneratedValue(generator = "user_generator")
    @GenericGenerator(name = "user_generator", strategy = "com.backend.Mock3project.generator.UserGenerator")
    private String id;

    @Nationalized
    @NotNull
    private String name;

    @Nationalized
    @NotNull
    private String sex;

    @NotNull
    @Temporal(TemporalType.DATE)
    private Date birth;

    @Column(name = "identity_card")
    @NotNull
    private String identityCard;

    @Column(name = "phone_number")
    @NotNull
    private String phoneNumber;

    @Nationalized
    @Size(max = 500)
    @NotNull
    private String address;

    @Nationalized
    private String specialization;

    @Nationalized
    private String experience;

    @NotNull
    private String email;

    @NotNull
    private String username;

    @JsonIgnore
    @NotNull
    private String password;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roles;

    @ManyToOne
    @JoinColumn(name = "branch_id", referencedColumnName = "id")
    private Branch branch;

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Schedule> schedules;

    @JsonIgnore
    private Integer flag;

    @Nationalized
    @Size(max = 500)
    private String note;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<UserDevice> userDevices;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<UserNotification> userNotifications;

}
