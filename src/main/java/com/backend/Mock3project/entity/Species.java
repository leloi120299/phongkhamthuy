package com.backend.Mock3project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Species {
    @Id
    @GeneratedValue(generator = "species_generator")
    @GenericGenerator(name = "species_generator", strategy = "com.backend.Mock3project.generator.SpeciesGenerator")
    @Column(name="id")
    private String id;

    @Nationalized
    private String name;

    @Nationalized
    private String note;

    @JsonIgnore
    private Integer flag;

    @ManyToOne
    @JoinColumn(name = "type_id",referencedColumnName = "id")
    private Type type;

    @JsonIgnore
    @ManyToMany(mappedBy = "species")
    private List<Pet> pets;

}
