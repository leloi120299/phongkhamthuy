package com.backend.Mock3project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private LocalDate date;

    @NotNull
    private LocalTime time;

    @Nationalized
    @NotNull
    private String symptom;

    @Size(max = 500)
    @Nationalized
    private String note;

    @NotNull
    private String status;

    @Column(name = "plan_doc_id")
    private String planDocId;

    @Column(name = "real_doc_id")
    private String realDocId;

    @JsonIgnore
    private Integer flag;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @NotNull
    private User user;

    @ManyToOne
    @JoinColumn(name = "pet_id", referencedColumnName = "id")
    @NotNull
    private Pet pet;

}
