package com.backend.Mock3project.exception;

import com.backend.Mock3project.response.Response;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ControllerAdvice
// This class for checking All validation,Exception when add, update,.. every entity
public class GlobalExceptionHandler {
    @ExceptionHandler({MethodArgumentNotValidException.class, EmptyResultDataAccessException.class,
                NullPointerException.class, Exception.class})
    public ResponseEntity<Response> yourExceptionHandler(MethodArgumentNotValidException e) {
        Map<String, String> errors = new HashMap<String, String>();

        BindingResult bindingResult = e.getBindingResult();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        for (FieldError fieldError : fieldErrors) {
            errors.put(fieldError.getField(), fieldError.getDefaultMessage());
        }
        String getFormatErrorResponseMessage = String.valueOf(errors.values()).
                substring(1, String.valueOf(errors.values()).length() - 1);

        return ResponseEntity.ok(Response.builder().message(getFormatErrorResponseMessage).status(400).build());
    }
}
