package com.backend.Mock3project.security;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.backend.Mock3project.entity.Branch;
import com.backend.Mock3project.entity.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Data
public class UserDetailsImpl implements UserDetails {
    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private String sex;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date birth;
    private String identityCard;
    private String phoneNumber;


    private String address;
    private String specialization;
    private String username;
    @JsonIgnore
    private String password;
    private Branch branch;
    private Collection<? extends GrantedAuthority> authorities;

    public UserDetailsImpl(String id, String name, String sex, Date birth, String identityCard
            , String phoneNumber, String username
            , String address, String password, Branch branch,
                           Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.birth = birth;
        this.identityCard = identityCard;
        this.phoneNumber = phoneNumber;
        this.username = username;
        this.address = address;
        this.password = password;
        this.branch = branch;
        this.authorities = authorities;
    }

    public static UserDetailsImpl build(User user) {
        List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
        return new UserDetailsImpl(
                user.getId(),
                user.getName(),
                user.getSex(),
                user.getBirth(),
                user.getIdentityCard(),
                user.getPhoneNumber(),
                user.getUsername(),
                user.getAddress(),
                user.getPassword(),
                user.getBranch(),
                authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }
}