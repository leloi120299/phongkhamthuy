package com.backend.Mock3project.security;

import com.backend.Mock3project.security.filter.CustomAuthenticationFilter;
import com.backend.Mock3project.security.filter.CustomAuthorizationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.backend.Mock3project.utils.Constants.LINK_API;
import static org.springframework.http.HttpMethod.*;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsService userDetailsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManagerBean());
        customAuthenticationFilter.setFilterProcessesUrl(LINK_API + "/login");
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests().antMatchers(LINK_API + "/login/**", LINK_API + "/user/token/refresh/**",LINK_API + "/notification/**", LINK_API+ "/user/forgot", LINK_API+ "/user/change", "/imgs/**", "/swagger-ui/**", "/swagger-ui.html", "/v3/api-docs/**").permitAll();
        http.authorizeRequests().antMatchers(GET, LINK_API + "/user/**",
                LINK_API + "/customer/**",
                LINK_API + "/pet/**",
                LINK_API + "/schedule/**",
                LINK_API + "/branch/**",
                LINK_API + "/species/**",
                LINK_API + "/type/**")
                .hasAnyAuthority("DOC", "EMP");
        http.authorizeRequests().antMatchers(POST, LINK_API + "/user/**",
                LINK_API + "/customer/**",
                LINK_API + "/pet/**",
                LINK_API + "/schedule/**",
                LINK_API + "/branch/**",
                LINK_API + "/species/**",
                LINK_API + "/type/**").hasAnyAuthority("EMP");
        http.authorizeRequests().antMatchers(PUT, LINK_API + "/user/**",
                LINK_API + "/customer/**",
                LINK_API + "/pet/**",
                LINK_API + "/schedule/**",
                LINK_API + "/branch/**",
                LINK_API + "/species/**",
                LINK_API + "/type/**").hasAnyAuthority("EMP");
        http.authorizeRequests().antMatchers(DELETE, LINK_API + "/user/**",
                LINK_API + "/customer/**",
                LINK_API + "/pet/**",
                LINK_API + "/schedule/**",
                LINK_API + "/branch/**",
                LINK_API + "/species/**",
                LINK_API + "/type/**").hasAnyAuthority("EMP");
        http.authorizeRequests().anyRequest().authenticated();
        http.addFilter(customAuthenticationFilter);
        http.addFilterBefore(new CustomAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
