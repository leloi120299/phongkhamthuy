package com.backend.Mock3project.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.backend.Mock3project.entity.Role;
import com.backend.Mock3project.entity.User;
import org.springframework.security.core.GrantedAuthority;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.stream.Collectors;

public class TokenGenerate {
    public static Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());

    /**
     * Generate access token
     * @UserDetailsImpl userSecurity
     * @User userEntity
     * @HttpServletRequest request
     * @Return JWT access token
     */
    public static String generateAccessToken(UserDetailsImpl userSecurity, User userEntity, HttpServletRequest request) {
        if (userEntity != null) {
            return JWT.create()
                    .withSubject(userEntity.getUsername())
                    .withExpiresAt(new Date(System.currentTimeMillis() + 240 * 60 * 1000))
                    .withIssuer(request.getRequestURL().toString())
                    .withClaim("roles", userEntity.getRoles().stream().map(Role::getName).collect(Collectors.toList()))
                    .sign(algorithm);
        }
        return JWT.create()
                .withSubject(userSecurity.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + 240 * 60 * 1000))
                .withIssuer(request.getRequestURL().toString())
                .withClaim("roles", userSecurity.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .sign(algorithm);
    }

    /**
     * Generate refresh token
     * @UserDetailsImpl user
     * @HttpServletRequest request
     * @Return JWT refresh token
     */
    public static String generateRefreshToken(UserDetailsImpl user, HttpServletRequest request) {
        return JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + 24 * 24 * 3600 * 1000))
                .withIssuer(request.getRequestURL().toString())
                .sign(algorithm);
    }
}
